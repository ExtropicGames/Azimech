package com.extropicstudios.azimech.scripts;

import static org.junit.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.extropicstudios.dyson.FormatString;

public class ScriptStringTest {

	FormatString fstring;
	
	@Before
	public void setUp() {
	}
	
	@Test
	public void basicTest() {
		Integer i = 10;
		List<Object> objectList = new ArrayList<Object>();
		objectList.add(i);
		fstring = new FormatString("Hey! %v is my favorite number!", objectList);
		System.out.println(fstring);
		assertTrue(fstring.toString().equals("Hey! 10 is my favorite number!"));
	}
	
	@Test
	public void startWithVariableTest() {
		Integer i = 10;
		Integer j = 20;
		List<Object> objectList = new ArrayList<Object>();
		objectList.add(i);
		objectList.add(j);
		fstring = new FormatString("%v is i. j is %v", objectList);
		System.out.println(fstring);
		assertTrue(fstring.toString().equals("10 is i. j is 20"));
	}
	
	@Test
	public void onlyVariablesTest() {
		Integer i = 867;
		Integer j = 530;
		Integer k = 9;
		List<Object> objectList = new ArrayList<Object>();
		objectList.add(i);
		objectList.add(j);
		objectList.add(k);
		fstring = new FormatString("%v%v%v", objectList);
		System.out.println(fstring);
		assertTrue(fstring.toString().equals("8675309"));
	}
}

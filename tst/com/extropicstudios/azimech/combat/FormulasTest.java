package com.extropicstudios.azimech.combat;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class FormulasTest {

	@Before
	public void setUp() {
		
	}
	
	@Test
	public void calculateHitChanceTest() {
		double chance = Formulas.calculateHitChance(100, 100, 10);
		System.out.println(chance);
		assertTrue(chance == 50.0d);
		chance = Formulas.calculateHitChance(200, 100, 10);
		System.out.println(chance);
		assertTrue(chance == 99.9d);
		chance = Formulas.calculateHitChance(100, 200, 10);
		System.out.println(chance);
		assertTrue(chance == 25.0d);
	}
}

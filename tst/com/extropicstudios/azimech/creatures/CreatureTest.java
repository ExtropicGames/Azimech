package com.extropicstudios.azimech.creatures;

import org.junit.Before;
import org.junit.Test;
import org.newdawn.slick.SlickException;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.singletons.SimpleManager;

public class CreatureTest {

	private static final SimpleManager sm = SimpleManager.getInstance();
	
	Creature creature;
	
	@Before
	public void setUp() {
		sm.unload();
		sm.loadFile("testdata/basic.xml", SimpleManager.Type.DATA);
		creature = new NPC();
		creature.initialize();
	}
	
	@Test
	public void useSkillTest() throws SlickException {
		sm.loadFile("testdata/CreatureTest/skills.xml", SimpleManager.Type.DATA);
		sm.initialize();
		double startRank = creature.getSkillRank("UNARMORED");
		creature.useSkill("UNARMORED", 1, 1);
		double endRank = creature.getSkillRank("UNARMORED");
		System.out.println("Start: " + startRank + " End: " + endRank);
		
		startRank = endRank;
		creature.useSkill("UNARMORED", 1, 1);
		endRank = creature.getSkillRank("UNARMORED");
		System.out.println("Start: " + startRank + " End: " + endRank);
	}
}

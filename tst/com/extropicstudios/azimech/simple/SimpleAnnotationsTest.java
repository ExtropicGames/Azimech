package com.extropicstudios.azimech.simple;

import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.singletons.SimpleManager;

public class SimpleAnnotationsTest {
	
	private SimpleManager sm;
	
	@Before
	public void setUp() {
		sm = SimpleManager.getInstance();
		sm.unload();
	}
	
	@Test
	public void basicNPCTest() {
		sm.loadFile("testdata/SimpleManagerTest/creatureTest.xml", SimpleManager.Type.DATA);
		Creature test = sm.getCreature("TEST");
		assertTrue(test.getName().equals("test"));
		assertTrue(test.x() == 10);
		assertTrue(test.y() == 15);
		//assertTrue(test.getAnimation("DONE").equals("TREE_DONE"));
		//assertTrue(test.getAnimation("MOVING").equals("SNAIL_MOVING"));
		//assertTrue(test.getAnimation("IDLE").equals("RIEDRAN_IDLE"));
		assertTrue(test.getDialogue().equals("DWARF_CHAT"));
		assertTrue(test.builder.AIID.equals("wander"));
		assertTrue(test.builder.relationIDs.get("PLAYER").equals(-50f));
		assertTrue(test.builder.skillIDs.get("Badass").equals(100d));
		assertTrue(test.builder.skillIDs.get("Asskicking").equals(1000d));
	}

}

Concept: A plot-focused tactical RPG with a focus on NPC interaction and development.

In Azimech, you play as a member of an alien species whose planet is being invaded by humans. Human technology is far more advanced than your own, but your species is not completely defenseless. As the one chosen to lead your species' resistance against the humans, you must choose whether to pursue a diplomatic option or a violent one. Success in the game depends on recruiting NPC followers who each have distinct personalities and opinions on how the resistance should be fought. Several branching plot lines make things more interesting.

Inspirations: Mount & Blade, Ogre Tactics, Final Fantasy Tactics

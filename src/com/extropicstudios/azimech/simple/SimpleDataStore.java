package com.extropicstudios.azimech.simple;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.util.Log;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import com.extropicstudios.azimech.creatures.NPC;
import com.extropicstudios.azimech.data.DialogueNode;
import com.extropicstudios.azimech.data.Skill;
import com.extropicstudios.azimech.data.Zone;
import com.extropicstudios.azimech.factions.Faction;
import com.extropicstudios.azimech.items.ItemDef;

@Root(name="data")
public class SimpleDataStore {

	// these maps can be read as-is
	
	@ElementMap(entry="creature", key="ID", attribute=true, inline=true, required=false)
	public Map<String, NPC> creatureMap;
	
	@ElementMap(entry="dialogue", key="ID", attribute=true, inline=true, required=false)
	public Map<String, DialogueNode> dialogueMap;
	
	@ElementMap(entry="zone", key="ID", attribute=true, inline=true, required=false)
	public Map<String, Zone> zoneMap;
	
	@ElementMap(entry="faction", key="ID", attribute=true, inline=true, required=false)
	public Map<String, Faction> factionMap;
		
	@ElementMap(entry="item", key="ID", attribute=true, inline=true, required=false)
	public Map<String, ItemDef> itemMap;
	
	@ElementMap(entry="int", key="ID", attribute=true, inline=true, required=false)
	public Map<String, Integer> intMap;
	
	@ElementMap(entry="string", key="ID", attribute=true, inline=true, required=false)
	public Map<String, String> stringMap;
	
	@ElementMap(entry="bool", key="ID", attribute=true, inline=true, required=false)
	public Map<String, Boolean> boolMap;
	
	@ElementMap(entry="double", key="ID", attribute=true, inline=true, required=false)
	public Map<String, Double> doubleMap;
	
	// these are items that require processing
	
	@ElementList(entry="skill", inline=true, required=false)
	protected List<SimpleSkill> simpleSkills;

	// these are items that have been processed
	
	public Map<String, Skill> skillMap;
	
	public SimpleDataStore() {
		creatureMap = new HashMap<String, NPC>();
		dialogueMap = new HashMap<String, DialogueNode>();
		zoneMap = new HashMap<String, Zone>();
		factionMap = new HashMap<String, Faction>();
		skillMap = new HashMap<String, Skill>();
		itemMap = new HashMap<String, ItemDef>();
		intMap = new HashMap<String,Integer>();
		stringMap = new HashMap<String, String>();
		boolMap = new HashMap<String, Boolean>();
		doubleMap = new HashMap<String, Double>();
		simpleSkills = new LinkedList<SimpleSkill>();
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("SimpleDataStore: {");
		
		result.append("Creatures: ");
		result.append(creatureMap.size());
		
		result.append(" Dialogues: ");
		result.append(dialogueMap.size());
		
		result.append(" Zones: ");
		result.append(zoneMap.size());
		
		result.append(" Factions: ");
		result.append(factionMap.size());
		
		result.append(" Skills: ");
		result.append(skillMap.size());
		
		result.append(" Items: ");
		result.append(itemMap.size());
		
		result.append(" Integers: ");
		result.append(intMap.size());
		
		result.append(" Strings: ");
		result.append(stringMap.size());
		
		result.append(" Booleans: ");
		result.append(boolMap.size());
		
		result.append(" Doubles: ");
		result.append(doubleMap.size());
		
		return result.toString(); 
	}
	
	public void merge(SimpleDataStore target) {
		this.creatureMap.putAll(target.creatureMap);
		this.dialogueMap.putAll(target.dialogueMap);
		this.zoneMap.putAll(target.zoneMap);
		this.factionMap.putAll(target.factionMap);
		this.skillMap.putAll(target.skillMap);
		this.itemMap.putAll(target.itemMap);
		this.intMap.putAll(target.intMap);
		this.stringMap.putAll(target.stringMap);
		this.boolMap.putAll(target.boolMap);
		this.doubleMap.putAll(target.doubleMap);
		this.simpleSkills.addAll(target.simpleSkills);
	}
	
	public void process() {
		invertSkillTree(simpleSkills, 0, null);
		simpleSkills = null;
	}
	
	private void invertSkillTree(List<SimpleSkill> skillList, int depth, Skill parent) {
		if (skillList == null)
			return;
		
		for (SimpleSkill s : skillList) {
			Skill skill = new Skill(s.ID, s.name, depth, parent);
			if (skillMap.get(skill.ID) != null)
				Log.warn("Duplicate skill entry " + skill.ID);
			skillMap.put(skill.ID, skill);
			invertSkillTree(s.children, depth+1, skill);
		}
	}
	
}
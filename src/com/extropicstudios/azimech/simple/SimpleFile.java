package com.extropicstudios.azimech.simple;

import org.simpleframework.xml.*;

@Root(name="file")
public class SimpleFile {

	@Attribute(name="type")
	public String type;
	@Text
	public String name;
	
	public SimpleFile(@Attribute(name="type")String newType, @Text String newName) {
		type = newType;
		name = newName;
	}
	
	public String toString() {
		return "SimpleFile {Name: " + name + " Type: " + type + "}";
	}
}

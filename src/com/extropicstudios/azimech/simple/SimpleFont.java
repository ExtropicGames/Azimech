package com.extropicstudios.azimech.simple;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

// This class contains no logic

@Root (name="properties")
public class SimpleFont {

	@Element
	private String path;
	@Attribute
	private int size;
	@Attribute(required=false)
	private boolean bold;
	@Attribute(required=false)
	private boolean italic;
	@Attribute(required=false)
	public int r;
	@Attribute(required=false)
	public int g;
	@Attribute(required=false)
	public int b;
	
	/*
	 * getters and setters live down here in the dungeon of ignoring
	 */
	public void setPath(String path) {
		this.path = path;
	}
	public String getPath() {
		return path;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getSize() {
		return size;
	}
	public void setBold(boolean bold) {
		this.bold = bold;
	}
	public boolean isBold() {
		return bold;
	}
	public void setItalic(boolean italic) {
		this.italic = italic;
	}
	public boolean isItalic() {
		return italic;
	}
}

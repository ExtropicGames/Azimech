package com.extropicstudios.azimech.simple;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="filelist")
public class SimpleFileList {

	@ElementList(inline=true)
	public List<SimpleFile> fileList;
	
	public SimpleFileList() {
		fileList = new ArrayList<SimpleFile>();
	}
}

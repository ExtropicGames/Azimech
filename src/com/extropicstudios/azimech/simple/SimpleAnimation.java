package com.extropicstudios.azimech.simple;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.singletons.SimpleManager;

@Root (name="properties")
public class SimpleAnimation {

	@Element
	private String path;
	@Attribute(name="tw", required=false)
	private int tileWidth;
	@Attribute(name="th", required=false)
	private int tileHeight;
	@Attribute(required=false)
	private int padding;
	@Attribute(name="duration", required=false)
	private int frameDuration;
	
	public SimpleAnimation() {
		SimpleManager sm = SimpleManager.getInstance();
		tileWidth = sm.getInt(Strings.TILE_WIDTH);
		tileHeight = sm.getInt(Strings.TILE_HEIGHT);
		this.padding = 0;
		frameDuration = Constants.DEFAULT_ANIM_FRAME_INTERVAL;
	}
	
	// dungeon of ignoring
	public void setPath(String path) {
		this.path = path;
	}
	public String getPath() {
		return path;
	}
	public void setTileWidth(int tileWidth) {
		this.tileWidth = tileWidth;
	}
	public int getTileWidth() {
		return tileWidth;
	}
	public void setTileHeight(int tileHeight) {
		this.tileHeight = tileHeight;
	}
	public int getTileHeight() {
		return tileHeight;
	}
	public void setPadding(int padding) {
		this.padding = padding;
	}
	public int getPadding() {
		return padding;
	}
	public void setFrameDuration(int frameDuration) {
		this.frameDuration = frameDuration;
	}
	public int getFrameDuration() {
		return frameDuration;
	}
}
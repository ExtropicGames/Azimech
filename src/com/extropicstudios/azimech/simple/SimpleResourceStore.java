package com.extropicstudios.azimech.simple;

import java.util.HashMap;
import java.util.Map;

import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

@Root(name="resources")
public class SimpleResourceStore {

	@ElementMap(entry="sound", key="ID", attribute=true, inline=true, required=false)
	public Map<String, String> soundPaths;
	
	@ElementMap(entry="image", key="ID", attribute=true, inline=true, required=false)
	public Map<String, String> imagePaths;
	
	@ElementMap(entry="animation", key="ID", attribute=true, inline=true, required=false)
	public Map<String, SimpleAnimation> animationPaths;
	
	@ElementMap(entry="map", key="ID", attribute=true, inline=true, required=false)
	public Map<String, String> tiledMapPaths;
	
	@ElementMap(entry="font", key="ID", attribute=true, inline=true, required=false)
	public Map<String, SimpleFont> fontPaths;
	
	public SimpleResourceStore() {
		soundPaths = new HashMap<String, String>();
		imagePaths = new HashMap<String, String>();
		animationPaths = new HashMap<String, SimpleAnimation>();
		tiledMapPaths = new HashMap<String, String>();
		fontPaths = new HashMap<String, SimpleFont>();
	}
	
	public void merge(SimpleResourceStore target) {
		this.soundPaths.putAll(target.soundPaths);
		this.imagePaths.putAll(target.imagePaths);
		this.animationPaths.putAll(target.animationPaths);
		this.tiledMapPaths.putAll(target.tiledMapPaths);
		this.fontPaths.putAll(target.fontPaths);
	}
}

package com.extropicstudios.azimech.simple;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="skill")
public class SimpleSkill {

	@Attribute
	public String ID;
	@Attribute
	public String name;
	
	@ElementList(inline=true, required=false)
	public List<SimpleSkill> children;
	
	public SimpleSkill() {
		children = new LinkedList<SimpleSkill>();
	}
}

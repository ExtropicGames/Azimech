package com.extropicstudios.azimech.factions;

import java.util.Map;
import java.util.List;

public interface Entity {
	
	public Entity getAllegiance(List<Entity> entities);
	
	public float getRelation(Entity entity);
	
	public Map<Entity, Float> getAllRelations();
	
	public void setRelation(Entity entity, Float relation);
	
	public float adjustRelation(Entity entity, Float relationAdjustment);
}

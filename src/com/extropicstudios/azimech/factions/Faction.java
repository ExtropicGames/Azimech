package com.extropicstudios.azimech.factions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.newdawn.slick.util.Log;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import com.extropicstudios.azimech.singletons.SimpleManager;

@Root(name="properties")
public class Faction implements Entity {
	
	@ElementMap(entry="relation", key="entityID", attribute=true, inline=true, required=false)
	Map<String, Float> relationIDs;
	
	Map<Entity, Float> relations;
	
	public Faction() {
		relationIDs = new HashMap<String, Float>();
		relations = new HashMap<Entity, Float>();
	}
	
	//TODO: WARNING! This doesn't check for duplicate entities yet!
	public void initialize() {
		SimpleManager sm = SimpleManager.getInstance();
		for (Entry<String, Float> entry : relationIDs.entrySet()) {
			Entity entity = sm.getCreature(entry.getKey());
			if (entity != null)
				relations.put(entity, entry.getValue());
			else {
				entity = sm.getFaction(entry.getKey());
				if (entity != null)
					relations.put(entity, entry.getValue());
				else {
					Log.warn("Misplaced reference to entity " + entry.getKey());
				}
			}
		}
		// free them up once we link them all together
		relationIDs = null;
	}
	
	public Entity getAllegiance(List<Entity> entities) {
		if (entities == null) {
			Log.warn("tried to get allegiance from nobody");
			return null;
		}
		Entity fav = entities.get(0);
		float highScore = relations.get(entities.get(0));
		for (Entity e : entities) {
			float relation = relations.get(e);
			if (relation >= highScore) {
				if (relation == highScore) {
					// special case???
				}
				fav = e;
				highScore = relation;
			}
		}
		return fav;
	}

	@Override
	public float adjustRelation(Entity entity, Float relationAdjustment) {
		float relation = relations.get(entity);
		return relations.put(entity, relation + relationAdjustment);
	}

	@Override
	public final float getRelation(final Entity entity) {
		return relations.get(entity);
	}
	
	@Override
	public final Map<Entity, Float> getAllRelations() {return relations;}

	@Override
	public void setRelation(final Entity entity, final Float relation) {
		relations.put(entity, relation);
	}

}

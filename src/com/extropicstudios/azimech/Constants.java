package com.extropicstudios.azimech;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.geom.Vector2f;

import com.extropicstudios.azimech.Enums.Direction;

public class Constants {
	
	// Tables!
	public static final Map<Direction, Vector2f> OFFSET;
	
	static {
		OFFSET = new HashMap<Direction, Vector2f>();
		OFFSET.put(Direction.NORTH, new Vector2f(0,-1));
		OFFSET.put(Direction.SOUTH, new Vector2f(0,1));
		OFFSET.put(Direction.EAST, new Vector2f(1,0));
		OFFSET.put(Direction.WEST, new Vector2f(-1,0));
		OFFSET.put(Direction.NORTHEAST, new Vector2f(1,-1));
		OFFSET.put(Direction.SOUTHEAST, new Vector2f(1,1));
		OFFSET.put(Direction.NORTHWEST, new Vector2f(-1,-1));
		OFFSET.put(Direction.SOUTHWEST, new Vector2f(-1,1));
	}
	
	// Metagame constants
	/** The game's version number. */
	public static final int VERSION = 115;
	/** The windowbar title. */
	public static final String TITLE_STRING = "Azimech Alpha " + VERSION;
	
	// True constants
	/** The speed at which things are animated. */
	public static final float SPEED = 0.2f;
	
	// Defaults
	/** The default animation to use. */
	public static final String DEFAULT_ANIM = "IDLE";
	public static final int DEFAULT_ANIM_FRAME_INTERVAL = 10;
	
	// State ID numbers.
	public static final int STATE_MAIN_MENU = 0;
	public static final int STATE_GAMEPLAY = 1;
	public static final int STATE_LOADING = 2;
	public static final int STATE_COMBAT = 3;
	
	public static final String DATA_INDEX_LOCATION = "data/index.xml";
}

package com.extropicstudios.azimech.data;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

// This class contains no logic

@Root(name="response")
public class DialogueResponse {
	
	@Element
	public String text;
	//public Script script;
	@Element(name="goesTo")
	public String dialogueLink;
}

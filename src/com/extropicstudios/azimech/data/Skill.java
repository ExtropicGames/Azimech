package com.extropicstudios.azimech.data;

public class Skill {
	public final String ID;
	public final String name;
	public final int depth;
	public final Skill parent;
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("{Skill ID: ");
		result.append(ID);
		
		result.append(" Name: ");
		result.append(name);
		
		result.append(" Depth: ");
		result.append(depth);
		
		result.append(" Parent: ");
		if (parent == null)
			result.append("null");
		else
			result.append(parent.toString());
		
		result.append("}");
		
		return result.toString();
	}
	
	public Skill(String ID, String name, int depth, Skill parent) {
		this.ID = ID;
		this.name = name;
		this.depth = depth;
		this.parent = parent;
	}
}
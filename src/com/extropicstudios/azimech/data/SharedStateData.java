package com.extropicstudios.azimech.data;

import java.util.List;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.creatures.Cursor;
import com.extropicstudios.azimech.creatures.Player;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.dyson.gui.Monitor;
import com.extropicstudios.dyson.gui.UIElement;

// This state contains no logic.

/**
 * This is a simple class that allows key pieces of data to be moved
 * back and forth between states. It is primarily used by 
 * {@link LoadingState}, {@link WalkingState}, and {@link CombatState}. 
 * @author joshua
 */
public class SharedStateData {
	
	public Player player;
	public Cursor cursor;
	
	public List<Monitor> monitors;
	
	public List<Entity> entityControllers;
	
	public UIElement activeUIElement;
	
	public String loadThisZone;
	public Zone currentZone;
	
	public int tileHeight;
	public int tileWidth;
	
	public Camera cam;
}

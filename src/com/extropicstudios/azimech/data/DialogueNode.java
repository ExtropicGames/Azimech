package com.extropicstudios.azimech.data;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="properties")
public class DialogueNode {
	
	@Element(name="portraitID")
	private final String portraitID;
	@Element(name="text")
	private final String text;
	@ElementList(inline=true)
	private List<DialogueResponse> responses;
	
	public DialogueNode(@Element(name="portraitID") String portraitID, 
			            @Element(name="text") String text) {
		this.portraitID = portraitID;
		this.text = text;
	}
	
	public void addResponse(final DialogueResponse response) {
		responses.add(response);
	}
	
	public final String getText() {return text;}
	public final String getPortraitID() {return portraitID;}
	public final List<DialogueResponse> getResponses() {return responses;}
}

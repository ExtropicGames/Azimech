package com.extropicstudios.azimech.data;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.tiled.TiledMap;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.scripts.MapScript;
import com.extropicstudios.azimech.scripts.MapTransition;
import com.extropicstudios.azimech.scripts.MapTrigger;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.azimech.singletons.SimpleManager;

// This class contains logic.

/**
 * A state that stores information about a zone such as which 
 * {@link TiledMap} it uses, a list of the {@link Creature}s in the zone, 
 * walls in the zone, and {@link MapTrigger}s in the zone. 
 * @author joshua
 */
@Root(name="properties")
public class Zone {
	
	@Element
	private String mapID;
	
	@ElementList(entry="creature", inline=true)
	private List<String> creatureIDList;
	
	private List<Creature> creatureList;
	
	private List<MapTrigger> triggers;
	private boolean[][] zoneWalls;
	
	private boolean initialized;
	
	public Zone() {
		creatureIDList = new ArrayList<String>();
		creatureList = new ArrayList<Creature>();
		triggers = new ArrayList<MapTrigger>();
	}
	
	public void initialize() {
		
		if (initialized == true)
			return;
		
		ResourceManager rm = ResourceManager.getInstance();
		
		int mapWidth = rm.getTiledMap(mapID).getWidth();
		int mapHeight = rm.getTiledMap(mapID).getHeight();
		zoneWalls = new boolean[mapWidth][mapHeight];
		
		TiledMap map = rm.getTiledMap(mapID);
		int objectGroupCount = map.getObjectGroupCount();
		for (int g = 0; g < objectGroupCount; g++) {
			int objectCount = map.getObjectCount(g);
			for (int o = 0; o < objectCount; o++) {
				String type = map.getObjectType(g, o);
				if (type.equals("transition"))
					triggers.add(new MapTransition(map, g, o));
				else if (type.equals("script"))
					triggers.add(new MapScript(map, g, o));
				else if (type.equals("obstacle")) {
					int x = map.getObjectX(g, o) / map.getTileWidth();
					int y = map.getObjectY(g, o) / map.getTileHeight();
					int w = map.getObjectWidth(g, o) / map.getTileWidth();
					int h = map.getObjectHeight(g, o) / map.getTileHeight();
					for (int c = x; c < x + w; c++) {
						for (int r = y; r < y + h; r++) {
							zoneWalls[c][r] = true;
						}	
					}			
				}
			}
			
			initialized = true;
		}
		
		SimpleManager sm = SimpleManager.getInstance();
		
		for (String s : creatureIDList) {
			creatureList.add(sm.getCreature(s));
		}
	}
	
	public Zone(@Element(name="mapID") String newMapID) {
		mapID = newMapID;
		creatureIDList = new ArrayList<String>();
		creatureList = new ArrayList<Creature>();
	}
	
	/**
	 * Returns true if there is a collision, false otherwise.
	 * @param c - This creature will be ignored when checking for
	 *            creature collisions.
	 * @param xOffset
	 * @param yOffset
	 * @return
	 */
	public boolean checkCollision(Creature c, int xOffset, int yOffset) {
		return (checkCreatureCollision(c, c.x() + xOffset, c.y() + yOffset) != null) ||
	       checkWallCollision(c.x() + xOffset, c.y() + yOffset);
	}
	
	public boolean checkWallCollision(int x, int y) {
		if (inBounds(x, y))
			return zoneWalls[x][y];
		return true;
	}
	
	public boolean inBounds(int x, int y) {
		return (x >= 0 && y >= 0 && x < zoneWalls.length && y < zoneWalls[0].length);
	}
	
	/**
	 * Returns the creature at the given coordinates, ignoring the
	 * creature passed in as a parameter.
	 * @param creature - The creature to ignore.
	 * @return Returns null if no creature is at the given location.
	 */
	public Creature checkCreatureCollision(Creature creature, int x, int y) {
		for(Creature c2 : creatureList) {
			if (creature != c2) {
				if (x == c2.x() && y == c2.y())
					return c2;
			}
		}
		return null;
	}
	
	public final String getMapID() {return mapID;}
	public final List<String> getCreatureIDList() {return creatureIDList;}
	public final List<Creature> getCreatureList() {return creatureList;}
	public final boolean[][] getWalls() {return zoneWalls;}
	public final List<MapTrigger> getTriggerList() {return triggers;}
	
}

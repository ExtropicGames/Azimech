package com.extropicstudios.azimech.gui;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.TabbedWindow;

public class ExamineMenu extends TabbedWindow {
	
	private static final SimpleManager sm = SimpleManager.getInstance();
	
	public ExamineMenu(ResourceContainer rc, Creature creature, float x, float y, float w, float h, Runnable exitAction) {
		super(rc, x, y, w, h);
		this.setExitAction(exitAction);
		this.addTab(sm.getString(Strings.EQUIPMENT), new EquipmentPanel(rc, creature));
		this.addTab(sm.getString(Strings.SKILLS), new SkillsPanel(rc, creature));
		this.addTab(sm.getString(Strings.RELATIONS), new RelationsPanel(rc, creature));
	}
	
	public ExamineMenu(ResourceContainer rc, Creature creature, Runnable exitAction) {
		this(rc, creature, 10,10,900,400, exitAction);
	}
}
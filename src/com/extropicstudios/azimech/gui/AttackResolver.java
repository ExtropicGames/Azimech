package com.extropicstudios.azimech.gui;

import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.combat.Formulas;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.items.Armor;
import com.extropicstudios.azimech.items.Body;
import com.extropicstudios.azimech.items.Weapon;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.gui.UIElement;
import com.extropicstudios.dyson.gui.WindowFrame;

public class AttackResolver {
//
//	private static final SimpleManager sm = SimpleManager.getInstance();
//	
//	private static final String FONT = Strings.ATTACK_FONT;
//	
//	Rectangle bounds;
//	
//	WindowFrame attackerPanel;
//	WindowFrame defenderPanel;
//	
//	WindowFrame weaponPanel;
//	WindowFrame targetPanel;
//	
//	List<Weapon> weaponChoices;
//	int selectedWeapon;
//	List<Body> targetChoices;
//	int selectedTarget;
//	
//	private Creature attacker;
//	private Creature defender;
//	
//	private boolean isHidden;
//	
//	public AttackResolver(IResourceContainer rc, Creature attacker, Creature defender, float parentWidth, float parentHeight) {
//		this.attacker = attacker;
//		this.defender = defender;
//		
//		weaponChoices = attacker.getWeapons();
//		targetChoices = defender.getBodyParts();
//		
//		int screenHeight = sm.getInt(Strings.SCREEN_HEIGHT);
//		int screenWidth = sm.getInt(Strings.SCREEN_WIDTH);
//		
//		float w = parentWidth;
//		float h = parentHeight * 0.40f;
//		float y = parentHeight - h;
//		
//		bounds = new Rectangle(0, y, w, h);
//		
//		attackerPanel = new WindowFrame(rc, 0, 0, parentWidth * 0.20f , h);
//		attackerPanel.setRounded(true);
//		attackerPanel.setBorder(5);
//		
//		float x = screenWidth - w;
//		
//		defenderPanel = new WindowFrame(rc, x, y, w, h);
//		defenderPanel.setRounded(true);
//		defenderPanel.setBorder(5);
//		
//		x = w;
//		w = screenWidth * 0.60f;
//		h = screenHeight * 0.20f;
//		
//		weaponPanel = new WindowFrame(rc, x, y, w, h);
//		weaponPanel.setRounded(true);
//		weaponPanel.setBorder(3);
//		
//		
//		y = y + h;
//		
//		targetPanel = new WindowFrame(rc, x, y, w, h);
//		targetPanel.setRounded(true);
//		targetPanel.setBorder(3);
//	}
//	
//	public Weapon getSelectedWeapon() {
//		return weaponChoices.get(selectedWeapon);
//	}
//	
//	public Body getTargetedBodyPart() {
//		return targetChoices.get(selectedTarget);
//	}
//
//	@Override
//	public void processInput(Input input) {
//		if (input.isKeyPressed(Input.KEY_ESCAPE)) {
//			return UIAction.EXIT;
//		} else if (input.isKeyPressed(Input.KEY_SPACE)) {
//			if (!weaponChoices.isEmpty())
//				return UIAction.START_ATTACK;
//		} else if (input.isKeyPressed(Input.KEY_Q)) {
//			if (selectedWeapon > 0)
//				selectedWeapon--;
//		} else if (input.isKeyPressed(Input.KEY_A)) {
//			if (selectedWeapon < weaponChoices.size()-1)
//				selectedWeapon++;
//		} else if (input.isKeyPressed(Input.KEY_S)) {
//			if (selectedTarget < targetChoices.size()-1)
//				selectedTarget++;
//		} else if (input.isKeyPressed(Input.KEY_W)) {
//			if (selectedTarget > 0)
//				selectedTarget--;
//		}
//		
//		return UIAction.NO_ACTION;
//	}
//
//	@Override
//	public void render(Graphics g, float xOffset, float yOffset) {
//		if (isHidden)
//			return;
//		
//		attackerPanel.render(g, xOffset + bounds.getX(), yOffset + bounds.getY());
//		defenderPanel.render(g, xOffset + bounds.getX(), yOffset + bounds.getY());
//		weaponPanel.render(g, xOffset + bounds.getX(), yOffset + bounds.getY());
//		targetPanel.render(g, xOffset + bounds.getX(), yOffset + bounds.getY());
//		
//		Weapon weapon = weaponChoices.get(selectedWeapon);
//		Body target = targetChoices.get(selectedTarget);
//		Armor armor = target.getArmor();
//		
//		Double hitChance = Formulas.creatureHitChance(attacker, defender, weapon);
//		
//		attackerPanel.drawString(0, 0, "Chance to hit:", FONT);
//		attackerPanel.drawString(0, attackerPanel.getFontHeight(), hitChance.toString() + "%", FONT);
//		attackerPanel.drawString(0, attackerPanel.getFontHeight() * 2, new Double(attacker.getSkillRank(weapon.getAccuracySkill())).toString(), FONT);
//		defenderPanel.drawString(0, 0, new Double(defender.getSkillRank(sm.getString(Strings.UNARMORED_SKILL))).toString(), FONT);
//		
//		weaponPanel.drawString(0, 0, weapon.getName(), FONT);
//		targetPanel.drawString(0, 0, target.toString(), FONT);
//		if (armor == null)
//			targetPanel.drawString(0, targetPanel.getFontHeight(), "Health remaining: " + target.getRemainingDurability(), FONT);
//		else
//			targetPanel.drawString(0, targetPanel.getFontHeight(), "Armor remaining: " + armor.getRemainingDurability(), FONT);
//	}
//
//	@Override
//	public void show() {isHidden=false;}
//	@Override
//	public void hide() {isHidden=true;}
//
//    @Override
//    public void addUIChild(UIElement child) {
//        // TODO Auto-generated method stub
//        
//    }
}

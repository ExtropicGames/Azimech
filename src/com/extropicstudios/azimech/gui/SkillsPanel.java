package com.extropicstudios.azimech.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.Skill;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.ProgressBar;
import com.extropicstudios.dyson.gui.TabPanel;
import com.extropicstudios.dyson.gui.WindowFrame;

public class SkillsPanel extends WindowFrame implements TabPanel {
	
    private static final boolean ROUNDED = true;
    private static final String FONT = Strings.SKILLS_FONT;
    
	private List<SkillBar> skills;
	
	private boolean hasCursor;
	
	public SkillsPanel(ResourceContainer rc, final Creature creature) {
	    super(rc, 0,0,0,0);
	    setRounded(ROUNDED);
		skills = new ArrayList<SkillBar>();
		
		for (Entry<Skill, Double> e : creature.getSkills().entrySet()) {
			if (e.getKey() != null)
				skills.add(new SkillBar(e.getKey().name, e.getValue()));
		}
	}
	
	@Override
	public void setBounds(float x, float y, float w, float h) {
	    super.setBounds(x, y, w, h);

		initialize();
	}
	
	public void initialize() {
		float xPosition = x();
		float yPosition = y();
		for (SkillBar s : skills) {
			s.bar = new ProgressBar(rc, xPosition, yPosition, 100,10);
			s.bar.setValues((long) s.rank, 5000);
			yPosition += 20;
			if (yPosition > y() + height()) {
				yPosition = y();
				xPosition += 150;
			}
		}
	}

	@Override
	public void processInput(Input input) {
		if (input.isKeyPressed(Input.KEY_W)) {
			input.clearKeyPressedRecord();
			hasCursor = false;
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		//UnicodeFont uniFont = ResourceManager.getInstance().getFont(font);
		
		float xPosition = 0;
		float yPosition = 0;
		for (SkillBar s : skills) {
			drawString(xPosition, yPosition, s.name, rc.getDefaultFont());
			s.bar.render(g, xPosition + xOffset, yPosition + yOffset);
			yPosition += 20;
			if (yPosition > y() + height()) {
				yPosition = y();
				xPosition += 150;
			}
		}
	}
	
	private class SkillBar {
		
		public SkillBar(String name, double rank) {
			this.rank = rank;
			this.name = name;
		}
		
		public ProgressBar bar;
		public double rank;
		public String name;
		
	}
	
	@Override
	public void giveCursor() {hasCursor = true;}
	@Override
	public boolean hasCursor() {return hasCursor;}
}

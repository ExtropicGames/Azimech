package com.extropicstudios.azimech.gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.items.Inventory;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.WindowFrame;

public class InventoryMenu extends WindowFrame {
	
    private static final float BORDER = 10;
    private static final boolean ROUNDED = true;
    
	private final Runnable exitAction;
	
	private Inventory inventory;
	
	public InventoryMenu(ResourceContainer rc, Inventory inventory, Runnable exitAction) {
	    super(rc, 10,10,990,490);
	    this.exitAction = exitAction;
	    this.setBorder(BORDER);
	    this.setRounded(ROUNDED);
		
	    this.inventory = inventory;
	}
	
	@Override
	public void processInput(Input input) {
		if (input.isKeyPressed(Input.KEY_SPACE)) {
			exitAction.run();
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		this.render(g, xOffset, yOffset);
	}
}

package com.extropicstudios.azimech.gui;

import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.items.Armor;
import com.extropicstudios.azimech.items.Body;
import com.extropicstudios.azimech.items.Weapon;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.TabPanel;
import com.extropicstudios.dyson.gui.WindowFrame;

public class EquipmentPanel extends WindowFrame implements TabPanel {

    private static final String FONT = Strings.EQUIPMENT_FONT;

	private Body baseBody;
	private boolean hasCursor;

	public EquipmentPanel(ResourceContainer rc, Creature creature) {
	    super(rc, 0,0,0,0);

		this.baseBody = creature.getBody();
	}

	@Override
	public void processInput(Input input) {
		if (input.isKeyPressed(Input.KEY_W)) {
			input.clearKeyPressedRecord();
			hasCursor = false;
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;

		renderEquipmentList(baseBody, 0, 0);
	}

	// TODO: rewrite this whole thing
	private int renderEquipmentList(Body body, int xOffset, int yOffset) {
		if (body == null) {
			return yOffset;
		}
		super.drawString(xOffset, yOffset, body.toString(), rc.getDefaultFont());
		yOffset += super.getFontHeight();
		List<Body> children = body.getBodyParts();
		for (Body b : children) {
			yOffset = renderEquipmentList(b, xOffset + 20, yOffset);
		}
		return yOffset;
	}

	@Override
	public void giveCursor() {hasCursor = true;}
	@Override
	public boolean hasCursor() {return hasCursor;}

}

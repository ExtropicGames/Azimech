package com.extropicstudios.azimech.gui;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.UnicodeFont;

import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.data.DialogueNode;
import com.extropicstudios.azimech.data.DialogueResponse;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.WindowFrame;

public class DialogueMenu extends WindowFrame {
    
    private static final SimpleManager sm = SimpleManager.getInstance();

    private static final boolean DIALOGUE_ROUNDED = true;
    private static final float DIALOGUE_BORDER = 10;
    private static final float DIALOGUE_PADDING = 5;
    private static final float LINE_SPACING = 5;
    
	private String font;
	private DialogueNode dialogue;
	
	private final Runnable exitAction;
	
	//private Rectangle textArea;
	
	public DialogueMenu(ResourceContainer rc, float x, float y, float w, float h, String dialogueID, Runnable exitAction) {
	    super(rc, x,y,w,h);
	    
	    this.exitAction = exitAction;
	    this.setRounded(DIALOGUE_ROUNDED);
	    this.setBorder(DIALOGUE_BORDER);
	    this.setPadding(DIALOGUE_PADDING);
		
		//textArea = new Rectangle(x+border+padding, y+border+padding, w-((border*2) + (padding*2)), h-((border*2) + (padding*2)));
		font = sm.getString(Strings.MENU_FONT);
		dialogue = sm.getDialogueNode(dialogueID);
	}
	
	public void setDialogue(String dialogueID) {
		dialogue = SimpleManager.getInstance().getDialogueNode(dialogueID);
	}
	
	@Override
	public void processInput(Input input) {
		if (dialogue == null) {
			exitAction.run();
		} else {
			int choice = 0;
			if (input.isKeyPressed(Input.KEY_1)) {
				choice = 1;
			} else if (input.isKeyPressed(Input.KEY_2)) {
				choice = 2;
			} else if (input.isKeyPressed(Input.KEY_3)) {
				choice = 3;
			} else if (input.isKeyPressed(Input.KEY_4)) {
				choice = 4;
			} else if (input.isKeyPressed(Input.KEY_5)) {
				choice = 5;
			} else if (input.isKeyPressed(Input.KEY_6)) {
				choice = 6;
			} else if (input.isKeyPressed(Input.KEY_7)) {
				choice = 7;
			} else if (input.isKeyPressed(Input.KEY_8)) {
				choice = 8;
			} else if (input.isKeyPressed(Input.KEY_9)) {
				choice = 9;
			}
			if (choice != 0)
				dialogue = SimpleManager.getInstance().getDialogueNode(dialogue.getResponses().get(choice-1).dialogueLink);
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		super.render(g, xOffset, yOffset);
		
		if (dialogue != null) {
			
			List<String> wordList = Arrays.asList(dialogue.getText().split(" "));
			float offset = canvasY();
			offset = drawTextWrapped(wordList, offset);
			
			List<DialogueResponse> responseList = dialogue.getResponses();
			for (int i = 0; i < responseList.size(); i++) {
				wordList = new LinkedList<String>();
				wordList.add((i+1) + ".");
				wordList.addAll(Arrays.asList(responseList.get(i).text.split(" ")));
				offset = drawTextWrapped(wordList, offset);
			}
			
		}
	}
	
	private float drawTextWrapped(List<String> words, float yOffset) {
		UnicodeFont uniFont = ResourceManager.getInstance().getFont(font);
		StringBuilder text = new StringBuilder();
		int index = 0;
		while (index < words.size()) {
			int prelength = text.length();
			text.append(words.get(index)).append(' ');
			if (uniFont.getWidth(text.toString()) > canvasW()) {
				text.delete(prelength, text.length());
				uniFont.drawString(canvasX(), yOffset, text.toString());
				yOffset += uniFont.getLineHeight() + LINE_SPACING;
				text = new StringBuilder();
			}
			index++;
		}
		uniFont.drawString(canvasX(), yOffset, text.toString());
		return yOffset + uniFont.getLineHeight() + LINE_SPACING;
	}
}

package com.extropicstudios.azimech.gui;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.dyson.assets.ResourceContainer;
import com.extropicstudios.dyson.gui.TabPanel;
import com.extropicstudios.dyson.gui.UIElement;

public class RelationsPanel extends UIElement implements TabPanel {
	
	private boolean hasCursor;
	
	public RelationsPanel(ResourceContainer rc, Creature creature) {
		super(rc);
	}
	
	@Override
	public boolean hasCursor() {
		return hasCursor;
	}

	@Override
	public void processInput(Input input) {
		if (input.isKeyPressed(Input.KEY_W)) {
			input.clearKeyPressedRecord();
			hasCursor = false;
		}
	}

	@Override
	public void render(Graphics g, float xOffset, float yOffset) {
		if (isHidden())
			return;
		
		// TODO Auto-generated method stub
	}

	@Override
	public void giveCursor() {
		hasCursor = true;
	}

	@Override
	public void setBounds(float x, float y, float w, float h) {
		// TODO Auto-generated method stub
		
	}

}

package com.extropicstudios.azimech.ai;

import java.util.Random;

import static com.extropicstudios.azimech.Enums.Direction.*;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.singletons.RandomManager;

/**
 * This package just causes the creature to move in a random direction every
 * turn.
 * @author joshua
 */
public class AIWander implements AITask {

	private int delay;
	
	@Override
	public void update(int delta, Creature me, SharedStateData d) {
		if (me.inCombat)
			return;
		delay += delta;
		if (delay > 1000) {
			delay = 0;
			Random rand = RandomManager.getInstance().getRandom();
			int move = rand.nextInt(4);
			if (move == 0) {
				me.move(NORTH, d.currentZone);
			} else if (move == 1) {
				me.move(SOUTH, d.currentZone);
			} else if (move == 2) {
				me.move(EAST, d.currentZone);
			} else if (move == 3) {
				me.move(WEST, d.currentZone);
			}
		}
	}

	@Override
	public boolean completed() {
		// Wander eternal 
		// The road of life is endless
		// Searching forever
		return false;
	}

}

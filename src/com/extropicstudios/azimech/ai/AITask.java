package com.extropicstudios.azimech.ai;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;

/**
 * An interface that defines a "task" consisting of a script defining an
 * action taken by an NPC.
 * @author joshua
 */
public interface AITask {

	public void update(int delta, Creature creature, SharedStateData d);
	
	public boolean completed();

}

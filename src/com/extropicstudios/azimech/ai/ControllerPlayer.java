package com.extropicstudios.azimech.ai;

import com.extropicstudios.azimech.factions.Entity;

public class ControllerPlayer extends Controller {
	
	public ControllerPlayer(Entity controller) {
		this.controller = controller;
		super.initialize();
	}

}

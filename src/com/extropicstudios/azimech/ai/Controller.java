package com.extropicstudios.azimech.ai;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.PathNode;
import com.extropicstudios.azimech.factions.Entity;

public abstract class Controller {

	protected Entity controller;
	
	protected List<Creature> controlledCreatures;
	
	protected void initialize() {
		controlledCreatures = new ArrayList<Creature>();
	}
	
	public void assignCreatures(List<Creature> allCreatures, List<Entity> allEntities) {
		for (Creature c : allCreatures) {
			if (c.getAllegiance(allEntities) == controller && !controlledCreatures.contains(c))
				controlledCreatures.add(c);
		}
	}
	
	public void startTurn() {
		for (Creature c : controlledCreatures) {
			c.state = Creature.State.WAITING;
		}
	}

	public boolean allCreaturesMoved() {
		for (Creature c : controlledCreatures) {
			if (c.state == Creature.State.DONE)
				return false;
		}
		return true;
	}
	
	public boolean controls(Creature c) {
		for (Creature c2 : controlledCreatures) {
			if (c == c2)
				return true;
		}
		return false;
	}

	public LinkedList<PathNode> getPath(Creature selectedCreature) {
		//TODO: need to add some pathfinding here
		return new LinkedList<PathNode>();
	}

	public Creature selectCreature() {
		for (Creature c : controlledCreatures) {
			if (c.state == Creature.State.WAITING)
				return c;
		}
		return null;
	}
}

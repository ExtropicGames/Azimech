package com.extropicstudios.azimech.combat;

/**
 * A class that handles combat-related game logic.
 * 
 * DEPRECATED
 * 
 * @author joshua
 */
public class CombatManager {
//	public void update(int delta) {
//		if (state == State.MOVING) {
//			// the creature is done moving, unselect it
//			if (selectedCreature.state == Creature.State.DONE) {
//				unselect();
//			} 
//		}
//		else if (state == State.SELECTED && turn == Turn.COMPUTER) {
//			cursor = controllers.get(activeTurn).getPath(selectedCreature);
//			move(); 
//		}
//		else if (turn == Turn.COMPUTER) {
//			selectedCreature = controllers.get(activeTurn).selectCreature();
//			state = State.SELECTED;
//			if (selectedCreature == null)
//				endTurn();
//		}
//		
//		// center the camera on the moving creature or the cursor
//		if (state == State.MOVING || state == State.ATTACKING)
//			d.cam.centerOnPoint(selectedCreature.pixelX(), selectedCreature.pixelY());
//		else if (turn == Turn.PLAYER)
//			d.cam.centerOnPoint(cursor.x(), cursor.y());
//		
//		// update every
//		for (Creature c : d.currentZone.getCreatureList()) {
//			c.update(delta, d);
//		}
//	}
//	
//	/**
//	 * Ends the current controller's turn and starts the next one.
//	 */
//	public void endTurn() {
//		activeTurn++;
//		if (activeTurn >= controllers.size())
//			activeTurn = 0;
//		unselect();
//		
//		controllers.get(activeTurn).startTurn();
//		if (controllers.get(activeTurn) == this.controllerPlayer)
//			turn = Turn.PLAYER;
//		else
//			turn = Turn.COMPUTER;
//	}
//
//	/**
//	 * Returns the creature at the current cursor location.
//	 */
//	public Creature getCreatureAtCursor() {
//		return d.currentZone.checkCreatureCollision(null, cursor.x(), cursor.y());
//	}
//	
//	/**
//	 * Returns true if the cursor defines a valid path.
//	 */
//	public boolean isPathValid() {
//		if (cursor instanceof CursorPath) {
//			if (((CursorPath) cursor).isPathValid())
//				return true;
//		}
//		return false;
//	}
//
//	/**
//	 * Causes the selected creature to attack the creature at
//	 * the current cursor location, using the specified weapon
//	 * and targeting the specified body part.
//	 * @param weapon - The weapon to use.
//	 * @param target - The body part to target.
//	 */
//	public void executeAttack(Weapon weapon, Body target) {
//		Creature defender = getCreatureAtCursor();
//
//		double hitChance = Formulas.creatureHitChance(selectedCreature, defender, weapon);
//		
//		double dieRoll = RandomManager.getInstance().percentChance();
//		if (dieRoll < hitChance) {
//			// calculate damage
//			double atkPower = weapon.getPower();
//			double armorRating = target.getRating();
//			double atkSkill = selectedCreature.getSkillRank(weapon.getPowerSkill());
//			
//			double damage = Formulas.calculateDamage(atkPower, armorRating, atkSkill);
//			
//			// apply damage to defender
//			target.takeDamage(damage);
//			
//			// grant skill xp to defender
//			Armor armor = target.getArmor();
//			if (armor != null)
//				defender.useSkill(armor.getSkill(), 1, 1);
//			else
//				defender.useSkill(sm.getString(Strings.UNARMORED_SKILL), 1, 1);
//			
//			// check defender's armor/bodypart to see if it is destroyed
//			defender.checkDamageStatus(target);
//			
//			// grant skill xp to attacker
//			selectedCreature.useSkill(weapon.getAccuracySkill(), 1, 1);
//			selectedCreature.useSkill(weapon.getPowerSkill(), 1, 1);
//		} else {
//			System.out.println("Fuck, you missed!");
//		}
//
//		// consume ammo
//		
//		selectedCreature.state = Creature.State.DONE;
//		unselect();	
//	}
//	
//	/**
//	 * Causes the selected creature to approach the targeted creature
//	 * and begins the "attack" state.
//	 */
//	public void startAttack() { 
//		path.removeLast();
//		if (move())
//			state = State.ATTACKING;
//	}
//	
//	/**
//	 * Moves the selected creature along the path.
//	 * @return
//	 */
//	public boolean move() {
//		if (cursor instanceof CursorPath) {
//			if (((CursorPath) cursor).isPathValid()) {
//				selectedCreature.addTask(new AIFollowPath(((CursorPath) cursor).getPath()));
//				selectedCreature.state = Creature.State.MOVING;
//				state = State.MOVING;
//				return true;
//			} else {
//				Log.error("Tried to move on invalid path.");
//				unselect();
//				return false;
//			}
//		}
//		return false;
//	}
//	
//	/**
//	 * Cancel out of an "attack" state...?
//	 */
//	public void cancel() {
//		if (path != null && !path.isEmpty())
//			selectedCreature.teleport(path.getFirst().x, path.getFirst().y);
//		unselect();
//	}
//	
//	public void render(Graphics g) {
//		ResourceManager rm = ResourceManager.getInstance();
//
//		// draw the path here...
//		if (state == State.SELECTED) {
//			for (PathNode node : path) {
//				// don't draw the first node in the path because
//				// we don't want to obscure the selected creature
//				if (path.peek() != node) {
//					if (node.valid)
//						g.drawAnimation(rm.getAnimation("PATH-VALID"), node.x*tileWidth - d.cam.x(), node.y*tileHeight - d.cam.y());
//					else
//						g.drawAnimation(rm.getAnimation("PATH-INVALID"), node.x*tileWidth - d.cam.x(), node.y*tileHeight - d.cam.y());
//				}
//			}
//		}
//	}
//
//	/**
//	 * Returns true if the creature has not taken an action yet
//	 * and is controlled by the player.
//	 * @param c
//	 * @return
//	 */
//	public boolean isCreatureSelectable(Creature c) {
//		return (c.state == Creature.State.WAITING) && (controllerPlayer.controls(c));
//	}
//	
//	/*TODO:
//	 *  Break this function apart into the various Cursor classes.
//	 */
//	/**
//	 * Attempts to move the cursor in the given direction.
//	 * @param direction
//	 */
//	public void moveCursor(Direction direction) {
//		state.moveCursor(Direction direction);
//		// if there's no creature selected, we don't care how the cursor
//		// moves or about drawing the path
//		if (state == State.NO_SELECTION) {
//			d.cursor.move(direction, d.currentZone);
//			return;
//		}
//		
//		if (path.isEmpty()) {
//			addPathNode(selectedCreature.x(), selectedCreature.y());
//		}
//		
//		// the coordinates of where the cursor is trying to move to
//		int x = (int) (d.cursor.x() + Constants.OFFSET.get(direction).x);
//		int y = (int) (d.cursor.y() + Constants.OFFSET.get(direction).y);
//		
//		// now we check if the player is trying to backtrack
//		
//		// remove the node under the cursor so we can see the
//		// previous node
//		PathNode tempNode = path.pollLast();
//		PathNode prevNode = path.peekLast();
//		path.add(tempNode);
//		
//		// if there is no previous node, this is a mildly annoying
//		// special case
//		if (prevNode == null) {
//			// if the cursor is moving back onto the selected
//			// creature, it's backtracking
//			if (x == selectedCreature.x() && y == selectedCreature.y()) {
//				if (d.cursor.move(direction, d.currentZone))
//					path.pollLast();
//				return;
//			} else {
//				// TODO: this code is duplicated 3 times and should
//				// be replaced by a function
//				// if the selected creature is out of moves, don't let the
//				// cursor move unless it's backtracking
//				if ((selectedCreature.getSpeed() - path.size()) <= 0) {
//					return;
//				}
//				
//				if (d.cursor.move(direction, d.currentZone))
//					addPathNode(x,y);
//				return;
//			}
//		}
//		// if this is true, the cursor is trying to backtrack
//		if (prevNode.x == x && prevNode.y == y) {
//			// if the cursor can't move, push the node back
//			// on the path
//			if (d.cursor.move(direction, d.currentZone)) {
//				path.pollLast();
//			}
//			return;
//		}
//		
//		// TODO: this code is duplicated 3 times and should
//		// be replaced by a function
//		// if the selected creature is out of moves, don't let the
//		// cursor move unless it's backtracking
//		if ((selectedCreature.getSpeed() - path.size()) <= 0) {
//			return;
//		}
//		
//		// if the location the cursor is trying to move to is
//		// on the path, but not the most recent node, don't let it move
//		if (isPointOnPath(x,y)) {
//			return;
//		}
//		// if we made it this far, the player is not backtracking, and
//		// nothing else (here) is preventing the cursor from moving.
//		if (d.cursor.move(direction, d.currentZone)) {
//			addPathNode(x,y);
//		}
//	}
//	
//	private void addPathNode(int x, int y) {
//		if (!isPointOnPath(x, y)) {
//			PathNode node = new PathNode();
//			node.x = x;
//			node.y = y;
//			if (d.currentZone.checkCollision(selectedCreature, x - selectedCreature.x(), y - selectedCreature.y())) {
//				node.valid = false;
//			} else {
//				node.valid = true;
//			}
//			path.add(node);
//		}
//	}
//	
//	private boolean isPointOnPath(int x, int y) {
//		for (PathNode node : path) {
//			if (x == node.x && y == node.y) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	public boolean isCreatureAttackable(Creature c) {
//		if (c == null || c == selectedCreature)
//			return false;
//		// do a bit of shuffling of the path because
//		// we want to see if the selected creature can get to
//		// the creature that they're trying to attack
//		PathNode target = path.pollLast();
//		boolean validPath = path.isValid();
//		path.add(target);
//		if (validPath) {
//			if (!controllers.get(activeTurn).controls(c)) {	
//				return true;
//			}
//		}
//	}
//
//	public Creature getSelectedCreature() {
//		return selectedCreature;
//	}
}
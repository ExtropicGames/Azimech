package com.extropicstudios.azimech.combat;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.Cursor;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.gui.AttackResolver;

public class Attacking implements CombatSubState {

	SharedStateData d;
	
	Creature attacker;
	
	Cursor cursor;
	
	private int range;
	private int startX;
	private int startY;
	
	private AttackResolver resolverUI; 
	
	public Attacking(Creature attacker, SharedStateData d) {
		// TODO: Let the player switch between weapons and
		// adjust the range based on the weapon being used.
		range = 4;
		this.attacker = attacker;
		this.d = d;
		
		cursor = d.cursor;
	}
	
	@Override
	public void processInput(Input input) {
//		if (resolverUI != null)
//			resolverUI.processInput(input);
	}

	@Override
	public void render(Graphics g, Camera cam) {
//		if (resolverUI != null)
//			resolverUI.render(g, 0, 0);
		
		// TODO: render cursor
		// TODO: render range overlay
	}

	@Override
	public void update(int delta) {
		// TODO Auto-generated method stub
		
	}
}

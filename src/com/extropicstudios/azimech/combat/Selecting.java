package com.extropicstudios.azimech.combat;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.Enums.Direction;
import com.extropicstudios.azimech.ai.Controller;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.gui.PopUpMenu;

public class Selecting implements CombatSubState {

	private static final SimpleManager sm = SimpleManager.getInstance();
	
	private SharedStateData d;
	private Controller activeController;
	
	private PopUpMenu menu;
	
	public Selecting(final SharedStateData d, final Controller activeController) {
		this.d = d;
		this.activeController = activeController;
	}
	
	@Override
	public void processInput(final Input input) {
		if (menu != null) {
			menu.processInput(input);
			return;
		}
		
		if (input.isKeyDown(Input.KEY_W)) {
			d.cursor.move(Direction.NORTH, d.currentZone);
		} else if (input.isKeyDown(Input.KEY_S)) {
			d.cursor.move(Direction.SOUTH, d.currentZone);
		} else if (input.isKeyDown(Input.KEY_D)) {
			d.cursor.move(Direction.EAST, d.currentZone);
		} else if (input.isKeyDown(Input.KEY_A)) {
			d.cursor.move(Direction.WEST, d.currentZone);
		} else if (input.isKeyPressed(Input.KEY_SPACE)) {			
			final int tileWidth = sm.getInt(Strings.TILE_WIDTH);
			final int tileHeight = sm.getInt(Strings.TILE_HEIGHT);
			final int screenWidth = sm.getInt(Strings.SCREEN_WIDTH);
			final int screenHeight = sm.getInt(Strings.SCREEN_HEIGHT);
			
//			menu = new PopUpMenu(rc, (d.cursor.x() * tileWidth) - d.cam.x(), (d.cursor.y() * tileHeight) - d.cam.y(), screenWidth, screenHeight);
//			
//			final Creature target = d.currentZone.checkCreatureCollision(null, d.cursor.x(), d.cursor.y());
//			
//			if (target != null) {
//				menu.addOption(Strings.ICON_POPUP_INFO, sm.getString(Strings.EXAMINE), UIAction.EXAMINE);
//				if (activeController.controls(target)) {
//					if (!target.hasMoved())
//						menu.addOption(Strings.ICON_POPUP_MOVE, sm.getString(Strings.START_MOVE), UIAction.START_MOVE);
//					if (!target.hasAttacked())
//						menu.addOption(Strings.ICON_POPUP_ATTACK, sm.getString(Strings.START_ATTACK), UIAction.START_ATTACK);
//				}
//			}
//			menu.addOption(Strings.ICON_POPUP_CANCEL, sm.getString(Strings.CANCEL), UIAction.EXIT);
		}
	}
	
	public Creature getCreature() {
		return d.currentZone.checkCreatureCollision(null, d.cursor.x(), d.cursor.y());
	}

	@Override
	public void render(final Graphics g, final Camera cam) {
	    d.cursor.render(g, cam);
	    
	    if (menu != null)
			menu.render(g, 0, 0);
	}

	@Override
	public void update(final int delta) {
		d.cursor.update(delta, d);
	}
}

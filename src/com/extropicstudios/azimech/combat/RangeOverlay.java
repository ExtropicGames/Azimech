package com.extropicstudios.azimech.combat;

import java.util.Set;
import java.util.TreeSet;

import com.extropicstudios.azimech.Enums.Direction;
import com.extropicstudios.azimech.data.SharedStateData;

public class RangeOverlay {

    public Set<Point> points;
    
    public RangeOverlay(int range, SharedStateData d, int startX, int startY) {
       points = getPoints(startX, startY, 0, range, Direction.NONE, d);
    }
    
    public Set<Point> getPoints(int x, int y, int steps, int range, Direction dir, SharedStateData d) {
        Set<Point> result = new TreeSet<Point>();
        
        if (!d.currentZone.checkCollision(null, x, y)) {
            result.add(new Point(x,y));
        }
        
        if (steps == range)
            return result;
        
        if (dir != Direction.NORTH)
            result.addAll(getPoints(y-1, x, steps+1, range, Direction.SOUTH, d));
        if (dir != Direction.SOUTH)
            result.addAll(getPoints(y+1, x, steps+1, range, Direction.NORTH, d));
        if (dir != Direction.EAST)
            result.addAll(getPoints(y, x+1, steps+1, range, Direction.WEST, d));
        if (dir != Direction.WEST)
            result.addAll(getPoints(y, x-1, steps+1, range, Direction.EAST, d));
        
        return result;
    }
    
    
    public class Point implements Comparable<Point> {
        public int x;
        public int y;
        public Point(int x, int y) { this.x = x; this.y = y;}
        
        @Override
        public int compareTo(Point that) {
            if (this.x == that.x)
                return this.y - that.y;
            return this.x - that.x;
        }
        
        @Override
        public boolean equals(Object that) {
            if (!(that instanceof Point))
                return false;
            if (this.compareTo((Point) that) == 0)
                return true;
            return false;
        }
    }
}

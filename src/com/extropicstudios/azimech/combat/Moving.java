package com.extropicstudios.azimech.combat;

import java.util.LinkedList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.Cursor;
import com.extropicstudios.azimech.creatures.PathNode;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.gui.PopUpMenu;

public class Moving implements CombatSubState {

	private static final SimpleManager sm = SimpleManager.getInstance();
	
	SharedStateData d;
	
	private final Runnable exitAction;
	
	private Cursor cursor;
	private Creature target;
	
	private int range;
	
	private boolean moveStarted;
	
	private LinkedList<PathNode> path;
	
	private PopUpMenu menu;
	
	public Moving(Creature target, SharedStateData d, Runnable exitAction) {
		this.exitAction = exitAction;
		this.d = d;
		this.target = target;
		cursor = d.cursor;
		//cursor.teleport(target.x(), target.y());
		
		range = target.getSpeed();
		path = new LinkedList<PathNode>();
	}

	@Override
	public void processInput(Input input) {
	    
	    // if the creature is moving, idle until he's done.
        // If the cursor is moving, we don't have to check anything.
	    if (cursor.isMoving() || target.isMoving())
	        return;
	    
	    if (target.hasMoved())
	        exitAction.run();
	    
	    if (moveStarted)
	        return;
		
		// Ok, he's done. Stop the move action.
		if (target.hasMoved())
			exitAction.run();
		
		// If there's a menu up, check it for actions.
		if (menu != null) {
			menu.processInput(input);
			// They want to move. Move them there if we can.
//			if (result == UIAction.END_MOVE) {
//				// 	move the creature along the path
//			    moveStarted = true;
//			    menu = null;
//				//target.addTask(new AIFollowPath(path));
//			} else if (result == UIAction.EXIT) {
//				menu = null;
//			}
		}
		
		int x = cursor.x();
		int y = cursor.y();
		boolean moving = false;
		// first get the location we're moving to
		if (input.isKeyDown(Input.KEY_A)) {
			x--;
			moving = true;
		} else if (input.isKeyDown(Input.KEY_W)) {
			y--;
			moving = true;
		} else if (input.isKeyDown(Input.KEY_D)) {
			x++;
			moving = true;
		} else if (input.isKeyDown(Input.KEY_S)) {
			y++;
			moving = true;
		} else if (input.isKeyPressed(Input.KEY_SPACE)) {
			final int tileWidth = sm.getInt(Strings.TILE_WIDTH);
			final int tileHeight = sm.getInt(Strings.TILE_HEIGHT);
			final int screenWidth = sm.getInt(Strings.SCREEN_WIDTH);
			final int screenHeight = sm.getInt(Strings.SCREEN_HEIGHT);
			
//			menu = new PopUpMenu((cursor.x() * tileWidth) - d.cam.x(), (cursor.y() * tileHeight) - d.cam.y(), screenWidth, screenHeight);
//			if (validatePath()) {
//				menu.addOption(Strings.ICON_POPUP_MOVE, sm.getString(Strings.END_MOVE), UIAction.END_MOVE);
//			}
//			menu.addOption(Strings.ICON_POPUP_CANCEL, sm.getString(Strings.CANCEL), UIAction.FINISH);
		}
		
		// This block of code is executed when the cursor is moved.
		if (moving) {
			final int oldX = cursor.x();
			final int oldY = cursor.y();
			// if the path is empty, the cursor is on the player
			if (path.isEmpty()) {
				if (cursor.moveTo(x,y))
					path.addLast(new PathNode(oldX, oldY, true));
			} else {
				// compare to the last node in chain
				// if is the last node in chain
				if (x == path.getLast().x() && y == path.getLast().y()) {
					// delete node if the cursor isn't already trying to move
					if (cursor.moveTo(x,y))
						path.removeLast();
				} else {
					// if path is not full
					if (path.size() < range && !inPath(x,y)) {
						if (cursor.moveTo(x,y))
							path.addLast(new PathNode(oldX, oldY));
					}
				}
			}		
		}
	}
	
	private boolean inPath(final int x, final int y) {
		for (PathNode node : path) {
			if (node.x() == x && node.y() == y) {
				return true;
			}
		}
		return false;
	}
	
	private boolean validatePath() {
		for (PathNode node : path) {
			if (node.valid == false)
				return false;
		}
		return true;
	}

	@Override
	public void render(Graphics g, Camera cam) {
		cursor.render(g, cam);
		
		for (PathNode node : path) {
			node.render(g, cam);
		}
		
		if (menu != null)
            menu.render(g, 0, 0);
	}

	@Override
	public void update(int delta) {
	    
	    // the creature should be told to move during this part
	    if (moveStarted && !target.isMoving()) {
	        if (path.isEmpty())
	            target.setMoved(true);
	        else {
	            PathNode nextNode = path.pop();
	            target.moveTo(nextNode.x(), nextNode.y());    
	        }
	    }
	    
		cursor.update(delta, d);
	}

}

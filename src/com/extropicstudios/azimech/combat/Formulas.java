package com.extropicstudios.azimech.combat;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.items.Weapon;

public class Formulas {

	/**
	 * Calculates the chance for one creature to hit another, using the 
	 * given weapon. Uses the {@link calculateHitChance} formula.
	 * @param attacker
	 * @param defender
	 * @param weapon
	 * @return
	 */
	public static double creatureHitChance(final Creature attacker, final Creature defender, final Weapon weapon) {
		double accuracy = weapon.getAccuracy();
		double mobility = defender.getMobility();
		double hitSkill = attacker.getSkillRank(weapon.getAccuracySkill());
		
		return calculateHitChance(accuracy, mobility, hitSkill);
	}
	
	/**
	 * Calculates the chance to hit the target based on the following
	 * formula: chance = ((accuracy * log10(attack skill)) / average mobility) * 50.0
	 * @param accuracy
	 * @param avgMobility
	 * @param atkSkill
	 * @return
	 */
	public static double calculateHitChance(final double accuracy, final double avgMobility, double atkSkill) {
		if (atkSkill <= 0)
			atkSkill = 1;
		double chance = ((accuracy * Math.log10(atkSkill)) / avgMobility) * 50.0d;
		if (chance <= 0)
			chance = 0.1d;
		else if (chance >= 100)
			chance = 99.9d;
		return chance;
	}
	
	/**
	 * Calculates the damage dealt to a target using the following
	 * formula: damage = (attack power * log10(attack skill)) / armor 
	 * @param atkPower
	 * @param armor
	 * @param atkSkill
	 * @return
	 */
	public static final double calculateDamage(final double atkPower, final double armor, double atkSkill) {
		if (atkSkill <= 0)
			atkSkill = 1;
		return (atkPower * Math.log10(atkSkill)) / armor;
	}
	
	/**
	 * Calculates mobility using the following formula: 
	 * mobility = mobility * log10(mobility skill)
	 * @param mobility
	 * @param mobilitySkill
	 * @return
	 */
	public static final double calculateMobility(final double mobility, double mobilitySkill) {
		if (mobilitySkill <= 0)
			mobilitySkill = 1;
		return mobility * Math.log10(mobilitySkill);		
	}
}

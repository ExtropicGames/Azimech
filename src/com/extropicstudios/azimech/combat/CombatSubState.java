package com.extropicstudios.azimech.combat;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Camera;

public interface CombatSubState {
	
	/**
	 * Cancels the state, going into
	 * the previous state.
	 * @return Returns the previous state.
	 */
	public void update(int delta);
	public void processInput(Input input);
	public void render(Graphics g, Camera cam);
}

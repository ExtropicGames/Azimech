package com.extropicstudios.azimech.combat;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.gui.ExamineMenu;
import com.extropicstudios.dyson.assets.ResourceContainer;

/**
 * This class basically acts as an Action wrapper for the
 * ExamineMenu class.
 * @author joshua
 *
 */
public class Examining implements CombatSubState {

	ExamineMenu menu;
	
	/**
	 * @param c - The creature to examine
	 */
	public Examining(ResourceContainer rc, Creature creature) {
		menu = new ExamineMenu(rc, creature, new Runnable() { public void run() { }});
	}

	@Override
	public void processInput(Input input) {
		menu.processInput(input);
	}

	@Override
	public void render(Graphics g, Camera cam) {
		menu.render(g, 0, 0);
	}

	@Override
	public void update(int delta) {
	}
}

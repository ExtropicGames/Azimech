package com.extropicstudios.azimech;

public class Strings {

	// XML tokens: index.xml
	public static final Object FILETYPE_DATA = "data";
	public static final Object FILETYPE_RESOURCE = "resources";
	
	// XML Tokens
	public static final String TILE_WIDTH = "TILE_WIDTH";
	public static final String TILE_HEIGHT = "TILE_HEIGHT";
	
	// XML Tokens: Fonts
	public static final String ATTACK_FONT = "MENU_FONT";
	public static final String EQUIPMENT_FONT = "MENU_FONT";
	public static final String MENU_FONT = "MENU_FONT";
	public static final String SKILLS_FONT = "MENU_FONT";
	public static final String POPUP_FONT = "MENU_FONT";
	public static final String LIST_FONT = "MENU_FONT";
	public static final String MONITOR_FONT = "MENU_FONT";
	
	// XML tokens: config.xml
	public static final String CONFIG_FILE = "data/config.xml";
	public static final String SCREEN_WIDTH = "ScreenResolutionX";
	public static final String SCREEN_HEIGHT = "ScreenResolutionY";
	public static final String FULLSCREEN = "Fullscreen";
	public static final String SHOW_FPS = "ShowFPS";
	
	// XML tokens: UI icons
	public static final String ICON_LIST_CURSOR = "ICON_LIST_CURSOR";
	public static final String ICON_POPUP_MOVE = "ICON_POPUP_MOVE";
	public static final String ICON_POPUP_CANCEL = "ICON_POPUP_CANCEL";
	public static final String ICON_POPUP_INFO = "ICON_POPUP_INFO";
	public static final String ICON_POPUP_ATTACK = "ICON_POPUP_ATTACK";
	
	// XML tokens: UI elements
	public static final String CURSOR_TILE = "CURSOR";
	public static final String PATH_VALID = "PATH-VALID";
	public static final String PATH_INVALID = "PATH-INVALID";
	
	// XML tokens: Translation strings
	public static final String EMPTY_SLOT = "EMPTY_SLOT";
	
	public static final String EQUIPMENT = "EQUIPMENT";
	public static final String RELATIONS = "RELATIONS";
	public static final String SKILLS = "SKILLS";

    public static final String END_MOVE = "END_MOVE";
    public static final String CANCEL = "CANCEL";
    public static final String EXAMINE = "EXAMINE";
    public static final String START_MOVE = "START_MOVE";
    public static final String START_ATTACK = "START_ATTACK";
    
	public static final String UNARMORED_MOBILITY = "UNARMORED_MOBILITY";
	public static final String UNARMORED_SKILL = "UNARMORED_SKILL";
	public static final String NO_SKILL = "NONE";	
}

package com.extropicstudios.azimech;

public class Enums {
	
	public enum Direction { NORTH, SOUTH, EAST, WEST, 
		NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST, NONE }
	
	public enum TriggerType { MAP_TRANSITION, REPUTATION_CHANGE }
}

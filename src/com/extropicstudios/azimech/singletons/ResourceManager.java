package com.extropicstudios.azimech.singletons;

import java.util.Map.Entry;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.simple.SimpleAnimation;
import com.extropicstudios.azimech.simple.SimpleFont;
import com.extropicstudios.azimech.simple.SimpleResourceStore;

import com.extropicstudios.dyson.slick.SlickResourceContainer;

public class ResourceManager {
	
	private static ResourceManager _instance = new ResourceManager();
	
	private SlickResourceContainer resourceContainer;
	
	private ResourceManager() {
		resourceContainer = new SlickResourceContainer();
	}
	
	public final static ResourceManager getInstance() {
		return _instance;
	}
	
	public void loadResources(SimpleResourceStore sr) throws SlickException {
		loadResources(sr, false);
	}
	
	public void loadResources(SimpleResourceStore sr, boolean deferred) throws SlickException {
	
		if (deferred)
			LoadingList.setDeferredLoading(true);
		
		// for each resource type, iterate through the list of paths
		// stored in SimpleManager and grab each resource
		for (Entry<String, String> e : sr.imagePaths.entrySet()) {
			loadImage(e.getKey(), e.getValue());
		}
		for (Entry<String, String> e : sr.soundPaths.entrySet()) {
			loadSound(e.getKey(), e.getValue());
		}
		for (Entry<String, String> e : sr.tiledMapPaths.entrySet()) {
			loadTiledMap(e.getKey(), e.getValue());
		}
		for (Entry<String, SimpleFont> e : sr.fontPaths.entrySet()) {
			loadFont(e.getKey(), e.getValue());
		}
		for (Entry<String, SimpleAnimation> e : sr.animationPaths.entrySet()) {
			loadAnimation(e.getKey(), e.getValue());
		}
	}
	
	public void loadImage(String identifier, String file) throws SlickException {
		resourceContainer.loadImage(file, identifier);
	}
	
	public Image getImage(String identifier) {
		return resourceContainer.getImage(identifier);
	}
	
	public void loadAnimation(String identifier, SimpleAnimation animation) throws SlickException {
		resourceContainer.loadAnimation(animation.getPath(), identifier, animation.getTileWidth(), animation.getTileHeight(), animation.getFrameDuration(), animation.getPadding());
	}
	
	public Animation getAnimation(String identifier) {
		return resourceContainer.getAnimation(identifier);
	}
 
	public void loadSound(String identifier, String file) throws SlickException {
		resourceContainer.loadSound(file, identifier);
	}
 
	public Sound getSound(String identifier) {
		return resourceContainer.getSound(identifier);
	}
	
	public void loadFont(String identifier, SimpleFont font) throws SlickException {
		resourceContainer.loadFont(font.getPath(), identifier, font.r, font.g, font.b);
	}
 
	public UnicodeFont getFont(String identifier) {
		return (UnicodeFont) resourceContainer.getFont(identifier);
	}
	
	public void loadTiledMap(String identifier, String file) throws SlickException {
		resourceContainer.loadMap(file, identifier);
	}
	
	public TiledMap getTiledMap(String identifier) {
		return resourceContainer.getMap(identifier);
	}
}

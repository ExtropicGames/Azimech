package com.extropicstudios.azimech.singletons;

import java.io.File;
import java.io.FileNotFoundException;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.util.Log;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.AttributeException;
import org.simpleframework.xml.core.ElementException;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.core.ValueRequiredException;

import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.DialogueNode;
import com.extropicstudios.azimech.data.Skill;
import com.extropicstudios.azimech.data.Zone;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.azimech.factions.Faction;
import com.extropicstudios.azimech.items.ItemDef;
import com.extropicstudios.azimech.simple.SimpleDataStore;
import com.extropicstudios.azimech.simple.SimpleFile;
import com.extropicstudios.azimech.simple.SimpleFileList;
import com.extropicstudios.azimech.simple.SimpleResourceStore;

public class SimpleManager {
	
	public enum Type {DATA, RESOURCE};
	
	private static SimpleManager _instance = new SimpleManager();
	
	private SimpleDataStore dataStore;
	private SimpleResourceStore resourceStore;
	
	private SimpleManager() {
		dataStore = new SimpleDataStore();
		resourceStore = new SimpleResourceStore();
	}
	
	public static SimpleManager getInstance() {
		return _instance;
	}
	
	public void saveData(final String filename) throws SlickException {
		Serializer serializer = new Persister();
		try {
			File result = new File(filename);
			serializer.write(dataStore, result);
		} catch (Exception e) {
			throw new SlickException("Could not save data", e);
		}
	}
	
	public boolean loadFile(final String filename, final Type type) {
		Serializer serializer = new Persister();
		File source = new File(filename);
		
		SimpleDataStore tempDataStore = new SimpleDataStore();
		SimpleResourceStore tempResourceStore = new SimpleResourceStore();
		try {
			source = new File(filename);
			if (source.canRead()) {
				if (type == Type.DATA) {
					tempDataStore = serializer.read(SimpleDataStore.class, source);
					dataStore.merge(tempDataStore);
				}
				else if (type == Type.RESOURCE) {
					tempResourceStore = serializer.read(SimpleResourceStore.class, source);
					resourceStore.merge(tempResourceStore);
				}
			} else {
				Log.error("Could not read from file: " + filename);
			}
		} catch (AttributeException e) {
			Log.error("XML Attribute error reading file " + filename + ": " + e.getMessage());
		} catch (ElementException e) {
			Log.error("XML Element error reading file " + filename + ": " + e.getMessage());
		} catch (FileNotFoundException e) {
			Log.error("File not found: " + filename + ": " + e.getMessage());
		} catch (ValueRequiredException e) {
			Log.error("File " + filename + " missing required value: " + e.getMessage());
		} catch (Exception e) {
			Log.error("Could not open file: " + filename + ": " + e.getMessage());
			e.printStackTrace();
		}
		dataStore.merge(tempDataStore);

		return true;
	}
	
	public void loadData() throws SlickException {
		Serializer serializer = new Persister();
		File source = new File(Constants.DATA_INDEX_LOCATION);

		SimpleFileList sList = null;
		
		try {
			sList = serializer.read(SimpleFileList.class, source);
		} catch (Exception e) {
			throw new SlickException("Error reading data from " + source.getPath(), e);
		}

		for(SimpleFile file : sList.fileList) {
			if (file.type.equals(Strings.FILETYPE_DATA))
				loadFile(file.name, Type.DATA);
			else if (file.type.equals(Strings.FILETYPE_RESOURCE))
				loadFile(file.name, Type.RESOURCE);
			else
				Log.error("Unrecognized file type " + file.type + " in " + source.getPath());
		}
		
		initialize();
	}
	
	public void initialize() throws SlickException {
		// now load complex data like images and sounds
		ResourceManager.getInstance().loadResources(resourceStore);
		// free up the memory here after we're done
		resourceStore = null;
		
		dataStore.process();
	}
	
	public void unload() {
		dataStore = new SimpleDataStore();
	}
	
	public Creature getCreature(String ID) {
		Creature result = dataStore.creatureMap.get(ID);
		if (result == null)
			Log.error("Missing creature ID " + ID);
		return result;
	}
	
	public DialogueNode getDialogueNode(String ID) {
		DialogueNode result = dataStore.dialogueMap.get(ID);
		if (result == null)
			Log.error("Missing dialogue ID " + ID);
		return result;
	}
	
	public Zone getZone(String ID) {
		Zone result = dataStore.zoneMap.get(ID);
		if (result == null)
			Log.error("Missing zone ID " + ID);
		return result;
	}
	
	public Faction getFaction(String ID) {
		Faction result = dataStore.factionMap.get(ID);
		if (result == null)
			Log.error("Missing faction ID " + ID);
		return result;
	}

	public Skill getSkill(String ID) {
		Skill result = dataStore.skillMap.get(ID);
		if (result == null)
			Log.error("Missing skill ID " + ID);
		return result;
	}
	
	public Integer getInt(String ID) {
		Integer result = dataStore.intMap.get(ID);
		if (result == null)
			Log.error("Missing int variable ID " + ID);
		return result;
	}
	
	public String getString(String ID) {
		String result = dataStore.stringMap.get(ID);
		if (result == null)
			Log.error("Missing string ID " + ID);
		return result;
	}
	
	public Boolean getBool(String ID) {
		Boolean result = dataStore.boolMap.get(ID);
		if (result == null)
			Log.error("Missing boolean variable ID " + ID);
		return result;
	}

	public ItemDef getItemDef(String ID) {
		ItemDef result = dataStore.itemMap.get(ID);
		if (result == null)
			Log.error("Missing item definition ID " + ID);
		return result;
	}

	public Double getDouble(String ID) {
		Double result = dataStore.doubleMap.get(ID);
		if (result == null)
			Log.error("Missing double variable ID " + ID);
		return result;
	}
	
	public Entity getEntity(String ID) {
		Entity result = dataStore.creatureMap.get(ID);
		if (result == null) {
			result = dataStore.factionMap.get(ID);
			if (result == null)
				Log.error("Missing entity ID " + ID);
		}
		return result;
	}
}

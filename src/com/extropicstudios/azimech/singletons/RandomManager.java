package com.extropicstudios.azimech.singletons;

import java.util.Random;

public class RandomManager {

	private static RandomManager _instance = new RandomManager();
	
	private Random rand;
	
	private RandomManager() {
		rand = new Random();
	}
	
	public final static RandomManager getInstance() {
		return _instance;
	}
	
	public double percentChance() {
		return rand.nextDouble() * 100;
	}
	
	public Random getRandom() {
		return rand;
	}
}

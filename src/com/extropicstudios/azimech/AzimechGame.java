package com.extropicstudios.azimech;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.azimech.states.CombatState;
import com.extropicstudios.azimech.states.LoadingState;
import com.extropicstudios.azimech.states.MainMenuState;
import com.extropicstudios.azimech.states.WalkingState;

/**
 * The entry point for the game. Responsible for initializing and running
 * the game and all its states.
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class AzimechGame extends StateBasedGame {
	
	public static SharedStateData data = new SharedStateData();
	
	public AzimechGame() {
		super(Constants.TITLE_STRING);
		this.addState(new MainMenuState(Constants.STATE_MAIN_MENU));
		this.addState(new WalkingState(Constants.STATE_GAMEPLAY, data));
		this.addState(new LoadingState(Constants.STATE_LOADING, data));
		this.addState(new CombatState(Constants.STATE_COMBAT, data));
		this.enterState(Constants.STATE_LOADING);
	}
	
	public void initStatesList(GameContainer gc) throws SlickException {
		Log.info("Entering AzimechGame.init()");
		// TODO: for some reason, LoadingState.init is getting called twice.
		//this.getState(Constants.STATE_LOADING).init(gc, this);
		this.getState(Constants.STATE_MAIN_MENU).init(gc, this);
		this.getState(Constants.STATE_GAMEPLAY).init(gc, this);
		this.getState(Constants.STATE_COMBAT).init(gc, this);
		Log.info("Exiting AzimechGame.init()");
	}
	
	public static void main(String[] args) throws SlickException {
		AppGameContainer app = new AppGameContainer(new AzimechGame());
		
		SimpleManager sm = SimpleManager.getInstance();
		
		if (!sm.loadFile(Strings.CONFIG_FILE, SimpleManager.Type.DATA))
			return;
		
		app.setDisplayMode(sm.getInt(Strings.SCREEN_WIDTH), sm.getInt(Strings.SCREEN_HEIGHT), sm.getBool(Strings.FULLSCREEN));
		app.setShowFPS(sm.getBool(Strings.SHOW_FPS));
		app.start();
	}

}

package com.extropicstudios.azimech;

import org.newdawn.slick.tiled.TiledMap;

/**
 * A camera that moves to follow the player.
 * @author joshua
 */
public class Camera {

	private float cameraX, cameraY;
	
	private int screenSizeX, screenSizeY;
	private int worldSizeX, worldSizeY;
	
	/**
	 * Resizes the dimension of the viewing area that this camera looks at.
	 * @param x
	 * @param y
	 */
	public void resizeScreen(int x, int y) {
		screenSizeX = x;
		screenSizeY = y;
	}
	
	/**
	 * Adjust the camera settings to optimize for viewing a certain map.
	 * This must be called every time the map changes.
	 * @param map - The map to view using this camera.
	 */
	public void setMap(TiledMap map) {
		worldSizeX = map.getWidth() * map.getTileWidth();
		worldSizeY = map.getHeight() * map.getTileHeight();
	}
	
	/**
	 * Center the camera on the given point.
	 * @param x
	 * @param y
	 */
	public void centerOnPoint(float x, float y) {
		// Put the camera left of the object, so the object ends up in the middle
		cameraX = x - (screenSizeX / 2);
		cameraY = y - (screenSizeY / 2);
		// Make sure the camera doesn't scroll off of the map
		if (cameraX < 0) 
			cameraX = 0;
		if (cameraX >= worldSizeX - screenSizeX) {
			cameraX =  worldSizeX - screenSizeX;
		}
		if (cameraY < 0)
			cameraY = 0;
		if (cameraY >= worldSizeY - screenSizeY) {
			cameraY =  worldSizeY - screenSizeY;
		}
	}
	
	public float x() {return cameraX;}
	public float y() {return cameraY;}
}

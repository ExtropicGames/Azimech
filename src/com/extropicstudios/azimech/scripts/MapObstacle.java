package com.extropicstudios.azimech.scripts;

import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;

public class MapObstacle extends MapTrigger {
	
	public MapObstacle(TiledMap map, int groupID, int objectID) {
		loadTriggerInfo(map, groupID, objectID);
	}

	/** It is an error if this is called, a creature should never be _in_ a
	 * MapObstacle. 
	 */
	@Override
	public void doTrigger(Creature c, SharedStateData data) {
		System.out.println("warning: creature " + c + " in obstacle " + name);
	}
}

package com.extropicstudios.azimech.scripts;

import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;

public class MapScript extends MapTrigger {
	private String scriptID;
	
	public MapScript(TiledMap map, int groupID, int objectID) {
		loadTriggerInfo(map, groupID, objectID);
		scriptID = map.getObjectProperty(groupID, objectID, "scriptID", "");
	}

	@Override
	public void doTrigger(Creature c, SharedStateData data) {
		// execute the script
	}

}

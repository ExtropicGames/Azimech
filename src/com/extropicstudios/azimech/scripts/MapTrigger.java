package com.extropicstudios.azimech.scripts;

import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;

public abstract class MapTrigger {
	/** The dimensions of the trigger, stored as tile coordinates. */
	protected Rectangle dimensions;
	protected String name;
	
	protected void loadTriggerInfo(TiledMap map, int groupID, int objectID) {	
		int x = map.getObjectX(groupID, objectID) / map.getTileWidth();
		int y = map.getObjectY(groupID, objectID) / map.getTileHeight();
		int w = map.getObjectWidth(groupID, objectID) / map.getTileWidth();
		int h = map.getObjectHeight(groupID, objectID) / map.getTileHeight();
		dimensions = new Rectangle(x, y, w, h);
		name = map.getObjectName(groupID, objectID);
	}
	
	public Rectangle getDimensions() {
		return dimensions;
	}
	
	/** 
	 * Returns true if the given shape collides with
	 * the map trigger. Calculations are performed as tile
	 * coordinates.
	 * @param shape - The Shape to check collision with.
	 * @return True if collision occurred.
	 */
	public boolean collides(Shape shape) {
		return dimensions.intersects(shape);
	}
	
	public boolean collides(int x, int y) {
		if (x < dimensions.getX())
			return false;
		if (y < dimensions.getY())
			return false;
		if (x > dimensions.getMaxX())
			return false;
		if (y > dimensions.getMaxY())
			return false;
		
		return true;
	}

	public abstract void doTrigger(Creature c, SharedStateData state);
}

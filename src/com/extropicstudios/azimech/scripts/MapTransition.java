package com.extropicstudios.azimech.scripts;

import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.Player;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.singletons.SimpleManager;

public class MapTransition extends MapTrigger {
	public String linkID;
	public int linkX;
	public int linkY;
	
	public MapTransition(TiledMap map, int groupID, int objectID) {
		loadTriggerInfo(map, groupID, objectID);
		linkID = map.getObjectProperty(groupID, objectID, "linkID", "");
		linkX = Integer.parseInt(map.getObjectProperty(groupID, objectID, "linkX", "0"));
		linkY = Integer.parseInt(map.getObjectProperty(groupID, objectID, "linkY", "0"));
	}
	
	@Override
	public void doTrigger(Creature c, SharedStateData data) {
		// transition triggers only affect players currently, although
		// it should be easy enough to extend to other creatures
		if (c instanceof Player) {
			SimpleManager sm = SimpleManager.getInstance();
			if (sm.getZone(linkID) == null) {
				System.out.println("Tried to go to invalid zone: " + linkID);
			} else {
				data.loadThisZone = linkID;
				c.teleport(linkX, linkY);
			}
		}
	}
	
}

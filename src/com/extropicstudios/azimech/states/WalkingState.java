package com.extropicstudios.azimech.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.tiled.TiledMap;

import static com.extropicstudios.azimech.Enums.Direction.*;
import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.Enums.Direction;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.gui.DialogueMenu;
import com.extropicstudios.azimech.scripts.MapTrigger;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.gui.Monitor;

public class WalkingState extends BasicGameState {
    private int stateID = -1;
    
	private SharedStateData d;
	
	private Creature selectedCreature;
	
	// singleton accessors
	private static final SimpleManager sm = SimpleManager.getInstance();
	private static final ResourceManager rm = ResourceManager.getInstance();
	
    public WalkingState(int newStateID, SharedStateData d) {
       this.stateID = newStateID;
       this.d = d;
    }
 
    @Override
    public int getID() {
        return stateID;
    }
 
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
	
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();

		if (d.activeUIElement == null) {
			if (input.isKeyDown(Input.KEY_W)) {
				d.player.move(NORTH, d.currentZone);
			} else if (input.isKeyDown(Input.KEY_A)) {
				d.player.move(WEST, d.currentZone);
			} else if (input.isKeyDown(Input.KEY_S)) {
				d.player.move(SOUTH, d.currentZone);
			} else if (input.isKeyDown(Input.KEY_D)) {
				d.player.move(EAST, d.currentZone);
			} else if (input.isKeyDown(Input.KEY_T)) {
				selectedCreature = getTalkTarget(d.player);
//				if (selectedCreature != null && selectedCreature.hasDialogue()) {
//					input.clearKeyPressedRecord();
//					selectedCreature.state = Creature.State.DONE;
//					DialogueMenu diag = new DialogueMenu(rc, 10,10,900,100, selectedCreature.getDialogue());
//					d.activeUIElement = diag;
//				}
			} else if (input.isKeyDown(Input.KEY_C)) {
				input.clearKeyPressedRecord();
				d.entityControllers.add(d.player);
				d.entityControllers.add(sm.getFaction("EVILS"));
				sbg.enterState(Constants.STATE_COMBAT, new FadeOutTransition(), new FadeInTransition());
			}
		} else {
            d.activeUIElement.processInput(input);
//            if (result == UIAction.EXIT) {
//                if (d.activeUIElement instanceof DialogueMenu) {
//                    selectedCreature.state = Creature.State.WAITING;
//                    selectedCreature = null;
//                }
//                d.activeUIElement = null;
//            }
        }
			
		d.cam.centerOnPoint(d.player.pixelX(), d.player.pixelY());
		
		for (Creature c : d.currentZone.getCreatureList()) {
			c.update(delta, d);
			checkTriggers(c);
		}
		
		if (d.loadThisZone != null) {
			sbg.enterState(Constants.STATE_LOADING);
		}
	}
	
	private Creature getTalkTarget(Creature c1) throws SlickException {
		Direction dir = c1.getFacing();
		if (dir == NORTH)
			return d.currentZone.checkCreatureCollision(c1, c1.x(), c1.y()-1);
		else if (dir == SOUTH)
			return d.currentZone.checkCreatureCollision(c1, c1.x(), c1.y()+1);
		else if (dir == EAST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()+1, c1.y());
		else if (dir == WEST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()-1, c1.y());
		else if (dir == NORTHEAST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()+1, c1.y()-1);
		else if (dir == SOUTHEAST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()+1, c1.y()+1);
		else if (dir == SOUTHWEST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()-1, c1.y()+1);
		else if (dir == NORTHWEST)
			return d.currentZone.checkCreatureCollision(c1, c1.x()-1, c1.y()-1);
		return null;
	}

	private void checkTriggers(Creature c) {
		for (MapTrigger t : d.currentZone.getTriggerList()) {
			if (t.collides(c.x(), c.y()))
				t.doTrigger(c, d);
		}
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		TiledMap map = rm.getTiledMap(d.currentZone.getMapID());
		
		map.render((int) -d.cam.x(), (int) -d.cam.y());
		
		for (Creature c : d.currentZone.getCreatureList()) {
			c.render(g, d.cam);
		}
		
		d.player.render(g, d.cam);
	
		for (Monitor m : d.monitors) {
			m.render(g,0,0);
		}
		
		if (d.activeUIElement != null)
			d.activeUIElement.render(g,0,0);
	}

}

package com.extropicstudios.azimech.states;


import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.Player;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.azimech.singletons.SimpleManager;
import com.extropicstudios.dyson.gui.Monitor;
import com.extropicstudios.dyson.gui.WindowFrame;

public class LoadingState extends BasicGameState {
	
    private int stateID = -1;
    private SharedStateData d;
    
    // static singleton accessors
    private static final SimpleManager sm = SimpleManager.getInstance();
    private static final ResourceManager rm = ResourceManager.getInstance();
    
    public LoadingState(int newStateID, SharedStateData d) {
       this.stateID = newStateID;
       this.d = d;
    }
 
    @Override
    public int getID() {
        return stateID;
    }

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		Log.info("Entering LoadingState.init()");
		gc.setVSync(true);
		
		// TODO: add a loading screen once deferred loading is in
		sm.loadData();
		
		d.monitors = new LinkedList<Monitor>();
		
		d.entityControllers = new LinkedList<Entity>();
		
		d.cam = new Camera();
		d.cam.resizeScreen(gc.getWidth(), gc.getHeight());
		
		d.player = new Player();
		d.player.cloneCreature(sm.getCreature("PLAYER"));
		
		//d.monitors.add(new Monitor(rc, "Player: ", d.player, gc.getWidth(), gc.getHeight()));
		
		d.loadThisZone = "RIEDRA_CITY1";
		
		//WindowFrame.setMenuFont(sm.getString(Strings.MENU_FONT));
		
		Log.info("Exiting LoadingState.init()");
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame sbg) {
		Log.info("Entering LoadingState.enter()");
		
		if (d.loadThisZone != null) {
			
			// unload the old zone
			// TODO: add code to offload the old zone before loading the new one
			if (d.currentZone != null)
				d.currentZone.getCreatureList().remove(d.player);
			
			// load the new zone
			
			d.currentZone = sm.getZone(d.loadThisZone);
			d.tileHeight = rm.getTiledMap(d.currentZone.getMapID()).getTileHeight();
			d.tileWidth = rm.getTiledMap(d.currentZone.getMapID()).getTileWidth();
			
			d.cam.setMap(rm.getTiledMap(d.currentZone.getMapID()));
			
			d.currentZone.initialize();			

			//d.cursor.initialize();
			
			d.currentZone.getCreatureList().add(d.player);
			
			for (Creature c : d.currentZone.getCreatureList()) {
				c.initialize();
			}
			
			d.loadThisZone = null;
			
		}
		
		sbg.enterState(Constants.STATE_GAMEPLAY);
	}

	@Override
	public void update(GameContainer gx, StateBasedGame sbg, int delta) throws SlickException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	

}

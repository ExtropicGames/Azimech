package com.extropicstudios.azimech.states;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.tiled.TiledMap;

import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.ai.Controller;
import com.extropicstudios.azimech.ai.ControllerPlayer;
import com.extropicstudios.azimech.ai.ControllerStupid;
import com.extropicstudios.azimech.combat.Attacking;
import com.extropicstudios.azimech.combat.CombatSubState;
import com.extropicstudios.azimech.combat.Examining;
import com.extropicstudios.azimech.combat.Moving;
import com.extropicstudios.azimech.combat.Selecting;
import com.extropicstudios.azimech.creatures.Creature;
import com.extropicstudios.azimech.creatures.Cursor;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.dyson.gui.Monitor;

public class CombatState extends BasicGameState {
    private int stateID = -1;
    private SharedStateData d;
    
    private static final ResourceManager rm = ResourceManager.getInstance();
    
    Stack<CombatSubState> substates;
    
    private List<Controller> controllers;
    int activeController;
    
    private List<Creature> creatureList;
    
    public CombatState(int newStateID, SharedStateData d) {
       this.stateID = newStateID;
       this.d = d;
    }
 
    @Override
    public int getID() {
        return stateID;
    }

    @Override
    public void enter(GameContainer gc, StateBasedGame sbg) {
    	controllers = new ArrayList<Controller>();
		creatureList = d.currentZone.getCreatureList();
		// create a controller for each entity participating in the combat
		for (Entity e : d.entityControllers) {
			if (e == d.player) {
				controllers.add(new ControllerPlayer(e));
			}
			else
				controllers.add(new ControllerStupid(e));
		}
		// let each controller look over the creature list and pick the
		// creatures who are allied with them
		for (Controller c : controllers) {
			c.assignCreatures(creatureList, d.entityControllers);
		}
    	
    	substates = new Stack<CombatSubState>();
    	substates.push(new Selecting(d, controllers.get(activeController)));
    	for (Creature c : d.currentZone.getCreatureList()) {
    		c.inCombat = true;
    		c.state = Creature.State.WAITING;
    	}
    	
    	d.cursor = new Cursor(d.player.x(), d.player.y());
    	
    }
    
    @Override
    public void leave(GameContainer gc, StateBasedGame sbg) {
    	for (Creature c : d.currentZone.getCreatureList()) {
    		c.inCombat = false;
    		c.state = Creature.State.WAITING;
    	}
    }
    
	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		Input input = gc.getInput();
		
		if (input.isKeyPressed(Input.KEY_C)) {
			input.clearKeyPressedRecord();
			sbg.enterState(Constants.STATE_GAMEPLAY, new FadeOutTransition(), new FadeInTransition());	
		}
		
		substates.peek().processInput(input);

//		if (substates.peek() instanceof Selecting) {
//			Creature target = ((Selecting) substates.peek()).getCreature(); 
////			if (result == UIAction.START_MOVE)
////				substates.push(new Moving(target, d));
////			else if (result == UIAction.START_ATTACK)
////				substates.push(new Attacking(target, d));
////			else if (result == UIAction.EXAMINE)
////				substates.push(new Examining(rc, target));
//			
//		} else if (substates.peek() instanceof Moving) {
//			if (result == UIAction.EXIT)
//				substates.pop();
//			
//		} else if (substates.peek() instanceof Attacking) {
//			if (result == UIAction.EXIT)
//				substates.pop();
//			
//		} else if (substates.peek() instanceof Examining) {
//			if (result == UIAction.EXIT)
//				substates.pop();
//		}
		
		if (substates != null && !substates.isEmpty()) {
			CombatSubState substate = substates.peek();
			if (substate != null)
				substate.update(delta);
		}
		
		if (creatureList != null) {
			for (Creature c : creatureList) {
				c.update(delta, d);
			}
		}
		
		for (Monitor m : d.monitors) {
			m.update();
		}
	}
	
	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		TiledMap map = rm.getTiledMap(d.currentZone.getMapID());
		
		map.render((int) -d.cam.x(), (int) -d.cam.y());
	
		if (creatureList != null) {
			for (Creature c : creatureList) {
				c.render(g, d.cam);
			}
		}
		
		if (substates != null) {
			CombatSubState substate = substates.peek();
			if (substate != null)
				substate.render(g, d.cam);
		}
		
		for (Monitor m : d.monitors) {
			m.render(g,0,0);
		}
	}

}
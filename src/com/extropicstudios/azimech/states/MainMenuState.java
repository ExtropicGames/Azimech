package com.extropicstudios.azimech.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class MainMenuState extends BasicGameState {

	private int stateID = -1;
	
	public MainMenuState(int newStateID) {
		this.stateID = newStateID;
	}
	
	public int getID() {
		return stateID;
	}
	
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
    	 
    }
 
    public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
 
    }
 
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
 
    }
}

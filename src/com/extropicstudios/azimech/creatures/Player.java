package com.extropicstudios.azimech.creatures;

import org.newdawn.slick.util.Log;

public class Player extends Creature {

	public String toString() {
		return super.toString();
	}
	
	public void cloneCreature(Creature other) {
		if (other == null) {
			Log.error("Tried to clone null creature.");
			return;
		}
		teleport(other.x(), other.y());
		this.facing = other.facing;
		this.animationMap = other.animationMap;
		this.baseBodyPart = other.baseBodyPart;
		this.inventory = other.inventory;
		this.name = other.name;
		this.relations = other.relations;
		this.speed = other.speed;
		this.builder = other.builder;
	}

	@Override
	public final String getDialogue() {return null;}
	@Override
	public final boolean hasDialogue() {return false;}
}

package com.extropicstudios.azimech.creatures;

import org.newdawn.slick.Graphics;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.singletons.ResourceManager;

public class Cursor extends Movable {
	
	protected static final ResourceManager rm = ResourceManager.getInstance();
	
	public Cursor() {
		super();
	}

	public Cursor(int x, int y) {
	    this();
	    teleport(x,y);
    }

    @Override
	public void render(final Graphics g, final Camera cam) {
		g.drawAnimation(rm.getAnimation(Strings.CURSOR_TILE), pixelX() - cam.x(), pixelY() - cam.y());
	}
}

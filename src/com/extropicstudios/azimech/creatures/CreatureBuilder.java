package com.extropicstudios.azimech.creatures;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.Map.Entry;

import org.newdawn.slick.util.Log;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementMap;

import com.extropicstudios.azimech.ai.AITask;
import com.extropicstudios.azimech.ai.AIWander;
import com.extropicstudios.azimech.data.Skill;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.azimech.singletons.SimpleManager;

public class CreatureBuilder {
	
	// singleton accessors
	private static final SimpleManager sm = SimpleManager.getInstance();
	
	@Element(name="AI", required=false)
	public String AIID;
	
	@Element(name="body", required=false)
	public String equipmentID;
	
	@ElementMap(entry="relation", key="entityID", attribute=true, inline=true, required=false)
	public Map<String, Float> relationIDs;
	
	@ElementMap(entry="skill", key="skillID", attribute=true, inline=true, required=false)
	public Map<String, Double> skillIDs;
	
	public CreatureBuilder() {
		relationIDs = new HashMap<String, Float>();
		skillIDs = new HashMap<String, Double>();
	}
	
	public Map<Entity, Float> buildRelationships() {
		Map<Entity, Float> result = new HashMap<Entity, Float>();
	
		for (Entry<String, Float> e : relationIDs.entrySet()) {
			Entity entity = sm.getCreature(e.getKey());
			if (entity != null)
				result.put(entity, e.getValue());
			else {
				entity = sm.getFaction(e.getKey());
				if (entity != null)
					result.put(entity, e.getValue());
				else {
					Log.warn("Misplaced reference to entity " + e.getKey());
				}
			}
		}
		
		return result;
	}
	
	public Map<Skill, Double> buildSkills() {
		Map<Skill, Double> result = new HashMap<Skill, Double>();
		
		for(Entry<String, Double> e : skillIDs.entrySet()) {
			result.put(sm.getSkill(e.getKey()), e.getValue());
		}
		
		return result;
	}
	
	public Stack<AITask> buildAI() {
		Stack<AITask> tasks = new Stack<AITask>();
		if (AIID != null) {
			if (AIID.equals("wander")) {
				tasks.push(new AIWander());
			}
		}
		return tasks;
	}
}

package com.extropicstudios.azimech.creatures;

import static com.extropicstudios.azimech.Enums.Direction.EAST;
import static com.extropicstudios.azimech.Enums.Direction.NORTH;
import static com.extropicstudios.azimech.Enums.Direction.SOUTH;
import static com.extropicstudios.azimech.Enums.Direction.WEST;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.simpleframework.xml.Attribute;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Constants;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.Enums.Direction;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.data.Zone;
import com.extropicstudios.azimech.singletons.SimpleManager;

public abstract class Movable {
	
	private static final SimpleManager sm = SimpleManager.getInstance();
	
	@Attribute(required=false)
	protected Direction facing; 
	
	@Attribute
	private int x;
	@Attribute
	private int y;
	
	private boolean isMoving;
	
	// these are related to animating the cursor
	private Vector2f position;
	private Vector2f inertia;
	
	public Movable() {
		position = new Vector2f();
		inertia = new Vector2f();
	}
	
	public String toString() {
		return "x: " + x + " y: " + y;
	}
	
	public boolean move(final Direction direction, final Zone zone) {
		if (!isMoving) {
			facing = direction;
			if (direction == NORTH) {
				if (zone.inBounds(x, y-1)) {
					setVectors(x, y, x, y-1);
					isMoving = true;
					y--;
					return true;
				}
			} else if (direction == WEST) {
				if (zone.inBounds(x-1, y)) {
					setVectors(x, y, x-1, y);
					isMoving = true;
					x--;
					return true;
				}
			} else if (direction == SOUTH) {
				if (zone.inBounds(x, y+1)) {
					setVectors(x, y, x, y+1);
					isMoving = true;
					y++;
					return true;
				}
			} else if (direction == EAST) {
				if (zone.inBounds(x+1, y)) {
					setVectors(x, y, x+1, y);
					isMoving = true;
					x++;
					return true;
				}
			}
			
			if (isMoving) {
				if (inertia.x == 0 && inertia.y == 0)
					isMoving = false;
			}
		}
		return false;
	}
	
	public void teleport(final int x, final int y) {
		this.x = x;
		this.y = y;
		inertia.set(0,0);
		final int h = sm.getInt(Strings.TILE_HEIGHT);
		final int w = sm.getInt(Strings.TILE_WIDTH);
		position.set(x * w, y * h);
		isMoving = false;
	}
	
	public boolean moveTo(final int x, final int y) {
		setVectors(this.x,this.y, x, y);
		this.x = x;
		this.y = y;
		isMoving = true;
		return true;
	}
	
	public void update(final int delta, final SharedStateData d) {

		final float ds = delta * Constants.SPEED;
		if (inertia.x < 0) {
			if ((ds + inertia.x) > 0) {
				position.x += inertia.x;
				inertia.x = 0;
			} else {
				position.x -= ds;
				inertia.x += ds;
			}
		} else if (inertia.x > 0) {
			if ((ds - inertia.x) > 0) {
				position.x += inertia.x;
				inertia.x = 0;
			} else {
				position.x += ds;
				inertia.x -= ds;
			}
		}
		if (inertia.y > 0) {
			if ((ds - inertia.y) > 0) {
				position.y += inertia.y;
				inertia.y = 0;
			} else {
				position.y += ds;
				inertia.y -= ds;
			}
		} else if (inertia.y < 0) {
			if ((ds + inertia.y) > 0) {
				position.y += inertia.y;
				inertia.y = 0;
			} else {
				position.y -= ds;
				inertia.y += ds;
			}
		}

		if (isMoving) {
			if (inertia.x == 0 && inertia.y == 0)
				isMoving = false;
		}
	}
	
	private void setVectors(final int startX, final int startY, final int endX, final int endY) {
		SimpleManager sm = SimpleManager.getInstance();
		final int h = sm.getInt(Strings.TILE_HEIGHT);
		final int w = sm.getInt(Strings.TILE_WIDTH);
		
		position.x = startX * w;
		position.y = startY * h;
		final float stopX = endX * w;
		final float stopY = endY * h;
		inertia.set(stopX - position.x, stopY - position.y);
	}

	public int x() {return x;}
	public int y() {return y;}
	public float pixelX() {return position.x;}
	public float pixelY() {return position.y;}
	public Direction getFacing() {return facing;}
	public boolean isMoving() {return isMoving;}
	
	public abstract void render(final Graphics g, final Camera cam);
}

package com.extropicstudios.azimech.creatures;

import static com.extropicstudios.azimech.Enums.Direction.EAST;
import static com.extropicstudios.azimech.Enums.Direction.NORTH;
import static com.extropicstudios.azimech.Enums.Direction.SOUTH;
import static com.extropicstudios.azimech.Enums.Direction.WEST;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.util.Log;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.Enums.Direction;
import com.extropicstudios.azimech.ai.AITask;
import com.extropicstudios.azimech.items.Armor;
import com.extropicstudios.azimech.items.Body;
import com.extropicstudios.azimech.items.Inventory;
import com.extropicstudios.azimech.items.Weapon;
import com.extropicstudios.azimech.combat.Formulas;
import com.extropicstudios.azimech.data.SharedStateData;
import com.extropicstudios.azimech.data.Skill;
import com.extropicstudios.azimech.data.Zone;
import com.extropicstudios.azimech.factions.Entity;
import com.extropicstudios.azimech.singletons.ResourceManager;
import com.extropicstudios.azimech.singletons.SimpleManager;

@Root(name="properties")
public abstract class Creature extends Movable implements Entity {
	
	private static final SimpleManager sm = SimpleManager.getInstance();
	private static final ResourceManager rm = ResourceManager.getInstance();
	
	/**
	 * <b>IDLE</b>      - The creature fulfills its tasks on its own 
	 *                    initiative.<br>
	 * <b>READY</b>     - The creature does nothing and waits for instructions.<br>
	 * <b>BUSY</b>      - The creature is doing something, like interacting in a 
	 *                    dialogue.<br>
	 * <b>MOVING</b>    - The creature is moving from one point to another.<br>
	 * <b>HAS_MOVED</b> - The creature is done moving.
	 */
	public enum State {WAITING, MOVING, DONE, DEAD}
	public boolean inCombat;
	
	//@Attribute
	//protected int x;
	//@Attribute
	//protected int y;
	@Element(required=false)
	protected String name;
	
	@Element(required=false)
	protected int speed;
	
	@ElementMap(entry="animation", key="type", attribute=true, inline=true)
	protected Map<String, String> animationMap;
	
	@Element(required=false)
	public CreatureBuilder builder;

	protected Map<Entity, Float> relations;
	protected Map<Skill, Double> skills;
	
	private Stack<AITask> tasks;
	
	@Element(name="body")
	protected Body baseBodyPart;
	
	@Element(required=false)
	protected Inventory inventory;
	
	public State state;
	
	private boolean hasMoved;
	private boolean hasAttacked;
	
	public Creature() {
		super();
		animationMap = new HashMap<String, String>();
		skills = new HashMap<Skill, Double>();
		state = State.WAITING;
		// default value
		speed = 5;
	}
	
	public String toString() {
		return super.toString();
	}
	
	public void initialize() {
		if (builder != null) {
			relations = builder.buildRelationships();
			skills = builder.buildSkills();
			tasks = builder.buildAI();
			builder = null;
		} else {
			relations = new HashMap<Entity, Float>();
			skills = new HashMap<Skill, Double>();
			tasks = new Stack<AITask>();
		}
		teleport(x(),y());
	}
	
	@Override
	public void update(int delta, SharedStateData d) {

		super.update(delta, d);
		
		if (!tasks.isEmpty()) {
			tasks.peek().update(delta, this, d);
			if (tasks.peek().completed())
				tasks.pop();
		}
	}
	
	public double getMobility() {
		return getTotalMobility(baseBodyPart) / getNumBodyParts(baseBodyPart);
	}
	
	private int getNumBodyParts(final Body body) {
		int result = 1;
		for (Body child : body.getBodyParts()) {
			result += getNumBodyParts(child);
		}
		return result;
	}
	
	private double getTotalMobility(final Body body) {
		double result;
		Armor armor = body.getArmor();
		if (armor == null) {
			result = Formulas.calculateMobility(sm.getDouble(Strings.UNARMORED_MOBILITY), 
					                   getSkillRank(sm.getString(Strings.UNARMORED_SKILL)));
		} else {
			result = Formulas.calculateMobility(armor.getMobility(), skills.get(armor.getSkill()));
		}
		for (Body child : body.getBodyParts()) {
			result += getTotalMobility(child);
		}
		return result;
	}
	
	@Override
	public float adjustRelation(Entity entity, Float relationAdjustment) {
		return relations.put(entity, getRelation(entity) + relationAdjustment);
	}
	
	@Override
	public boolean move(final Direction direction, final Zone zone) {
		// check for wall and creature collision
		if (direction == NORTH)
			if (zone.checkCollision(this, 0, -1))
				return false;
		if (direction == SOUTH)
			if (zone.checkCollision(this, 0, 1))
				return false;
		if (direction == EAST)
			if (zone.checkCollision(this, 1, 0))
				return false;
		if (direction == WEST)
			if (zone.checkCollision(this, -1, 0))
				return false;
		// then use the basic Movable move code
		return super.move(direction, zone);
	}
	
	/**
	 * Given a list of entities, picks the entity that this
	 * creature has the highest relation with.
	 */
	public Entity getAllegiance(List<Entity> entities) {
		if (entities == null || entities.isEmpty()) {
			Log.warn("tried to get allegiance from nobody");
			return null;
		}
		
		if (relations == null) {
			Log.warn("Creature has no relations defined.");
			return null;
		}
		
		Entity fav = entities.get(0);
		Float highScore = getRelation(entities.get(0));
		for (Entity e : entities) {
			// we are always allied to ourself.
			// NO EMOS ALLOWED
			if (e == this)
				return this;
			Float relation = getRelation(e);
			if (relation >= highScore) {
				if (relation == highScore) {
					// special case???
				}
				fav = e;
				highScore = relation;
			}
		}
		return fav;
	}
	
	/**
	 * Causes the creature to gain experience from
	 * using a specific skill.
	 * @param skillID
	 * @param xpGain
	 * @param multiplier
	 */
	public void useSkill(String skillID, float xpGain, float multiplier) {
		if (skillID.equals(Strings.NO_SKILL))
			return;
		useSkill(SimpleManager.getInstance().getSkill(skillID), xpGain, multiplier);
	}
	
	/**
	 * Causes the creature to gain experience from
	 * using a specific skill.
	 * @param skill
	 * @param xpGain
	 * @param multiplier
	 */
	public void useSkill(Skill skill, float xpGain, float multiplier) {
		if (skill == null)
			return;
		
		Double result = skills.get(skill);
		double increase = xpGain * multiplier;
		if (result == null) {
			result = new Double(increase);
		} else {
			result += increase;
		}
		skills.put(skill, result);
		
		useSkill(skill.parent, xpGain, multiplier / 2);
	}

	public void addTask(final AITask task) {tasks.push(task);}
	
	public void addAnimation(final String key, final String value) {
		animationMap.put(key, value);
	}
	
	@Override
	public void setRelation(Entity entity, Float relation) {
		relations.put(entity, relation);
	}
	
	@Override
	public float getRelation(Entity entity) {
		Float relation = relations.get(entity);
		if (relation == null) {
			relation = new Float(0);
			relations.put(entity, relation);
		}
		return relation;
	}
	
	@Override
	public Map<Entity, Float> getAllRelations() {return relations;}
	public State getState() {return state;}
	public Inventory getInventory() {return inventory;}
	public Map<Skill, Double> getSkills() {return skills;}
	public String getName() {return name;}
	
	public int getSpeed() {return speed;}
	public void setMoved(boolean hasMoved) {this.hasMoved = hasMoved;}
	public void setAttacked(boolean hasAttacked) {this.hasAttacked = hasAttacked;}
	public boolean hasMoved() {return hasMoved;}
	public boolean hasAttacked() {return hasAttacked;}
	
	private String getAnimation(final String animID) {
		return animationMap.get(animID);
	}
	
	public abstract boolean hasDialogue();
	public abstract String getDialogue();
	
	public double getSkillRank(String skillID) {
		final Skill skill = sm.getSkill(skillID);
		Double result = skills.get(skill);
		if (result == null) {
			result = new Double(0);
			skills.put(skill, result);
		}
		return result;
	}
	
	public void setSkillRank(String skillID, double rank) {
		final Skill skill = sm.getSkill(skillID);
		skills.put(skill, rank);
	}

	public List<Body> getBodyParts() {
		final List<Body> result = new ArrayList<Body>();
		getBodyParts(baseBodyPart, result);
		return result;
	}
	
	/**
	 * Helper function for the public getBodyParts.
	 */
	private void getBodyParts(final Body body, final List<Body> list) {
		list.add(body);
		for (Body child : body.getBodyParts()) {
			getBodyParts(child, list);
		}
	}
	
	public List<Weapon> getWeapons() {
		final List<Weapon> result = new ArrayList<Weapon>();
		getWeapons(baseBodyPart, result);
		return result;
	}
	
	/**
	 * Helper function for the public getWeapons
	 */
	private void getWeapons(final Body body, final List<Weapon> list) {
		final Weapon weapon = body.getWeapon();
		if (weapon != null)
			list.add(weapon);
		for (Body child : body.getBodyParts()) {
			getWeapons(child, list);
		}
	}
		
	public void checkDamageStatus(Body bodyPart) {
		Armor armor = bodyPart.getArmor();
		if (armor != null && bodyPart.getArmor().isDestroyed()) {
			// right now destroyed armor just evaporates into the ether
			bodyPart.setArmor(null);
		}
		
		if (bodyPart.isDestroyed()) {
			// drop all children
			// children should be checked for life criticalness too
			if (bodyPart.isLifeCritical() || bodyPart == baseBodyPart) {
				state = State.DEAD;
				// die
			}
		}
	}

	public void render(Graphics g, Camera cam) {
		g.drawAnimation(rm.getAnimation(getAnimation("IDLE")), pixelX() - cam.x(), pixelY() - cam.y());
	}
	
	public Body getBody() {return baseBodyPart;}
}

package com.extropicstudios.azimech.creatures;

import org.newdawn.slick.Graphics;

import com.extropicstudios.azimech.Camera;
import com.extropicstudios.azimech.Strings;
import com.extropicstudios.azimech.singletons.ResourceManager;

public class PathNode extends Movable {
	
	private static final ResourceManager rm = ResourceManager.getInstance();
	
	/**
	 * Whether the node is on an unblocked tile (valid) or a blocked one
	 * (invalid).
	 */
	public boolean valid;
	public boolean invisible;
	
	/**
	 * Creates a new PathNode at the given coordinates.
	 * {@link valid} defaults to true.
	 * @param x
	 * @param y
	 */
	public PathNode(int x, int y) {
		teleport(x,y);
		valid = true;
	}
	
	public PathNode(int x, int y, boolean invisible) {
		this(x,y);
		this.invisible = invisible;
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		
		result.append("{PathNode x:");
		result.append(x());
		
		result.append(" y:");
		result.append(y());
		if (valid)
			result.append(" valid}");
		else
			result.append(" invalid}");
		
		return result.toString();
	}

	public void render(Graphics g, Camera cam) {
		if (invisible)
			return;
		if (valid)
			g.drawAnimation(rm.getAnimation(Strings.PATH_VALID), pixelX() - cam.x(), pixelY() - cam.y());
		else
			g.drawAnimation(rm.getAnimation(Strings.PATH_INVALID), pixelX() - cam.x(), pixelY() - cam.y());
		
	}
}

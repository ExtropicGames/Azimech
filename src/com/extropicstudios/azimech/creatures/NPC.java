package com.extropicstudios.azimech.creatures;

import org.newdawn.slick.Graphics;
import org.simpleframework.xml.Element;

import com.extropicstudios.azimech.Camera;

public class NPC extends Creature {

	/** The dialogue triggered when this creature is talked to. (This should be in the NPC class.) */
	@Element(name="dialogue", required=false)
	private String dialogueID;
	
	@Override
	public final String getDialogue() {return dialogueID;}
	@Override
	public final boolean hasDialogue() {return dialogueID != null;}
}

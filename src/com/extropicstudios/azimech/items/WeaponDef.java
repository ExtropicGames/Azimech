package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Element;

public class WeaponDef extends EquipmentDef {

	@Element
	public double power;
	@Element
	public double accuracy;
	@Element
	public String accuracySkill;
	@Element
	public String powerSkill;
}

package com.extropicstudios.azimech.items;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

import com.extropicstudios.azimech.singletons.SimpleManager;

/**
 * A Body is a type of {@link Equipment} that can potentially have
 * one {@link Armor} and one {@link Weapon} attached to it,
 * and any number of sub-Bodies. The actual possibility of
 * these items being usable by this Body part is determined
 * by the {@link BodyDef} for this Body.
 * 
 * @author Joshua Stewart <chuzzum@gmail.com>
 */
public class Body extends Equipment {
	
	private static final SimpleManager sm = SimpleManager.getInstance();
	
	@Element(required=false)
	protected Armor armor;
	
	@Element(required=false)
	protected Weapon weapon;
	
	@ElementList(entry="bodypart", inline=true, required=false)
	protected List<Body> bodyParts;
	
	
	
	public Body() {
		super();
		bodyParts = new LinkedList<Body>();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getName());
		sb.append(" (");
		if (armor == null)
			sb.append("unarmored");
		else
			sb.append(armor.getName());
		sb.append(")");
		if (weapon != null) {
			sb.append(", holding ");
			sb.append(weapon.getName());
		}
		return sb.toString();
	}
	
	public void setArmor(final Armor armor) {
		this.armor = armor;
	}
	
	public Armor getArmor() {
		return armor;
	}
	
	public Weapon getWeapon() {
		return weapon;
	}
	
	@Override
	public double takeDamage(final double damage) {
		if (armor != null) {
			return super.takeDamage(armor.takeDamage(damage));
		} else {
			return super.takeDamage(damage);
		}
	}
	
	public boolean isLifeCritical() {
		return ((BodyDef) sm.getItemDef(itemDef)).lifeCritical;
	}
	
	public List<Body> getBodyParts() {
		return bodyParts;
	}
	
	public Slot getArmorSlot() {
		return ((BodyDef) sm.getItemDef(itemDef)).armorSlot;
	}
	
	public Slot getWeaponSlot() {
		return((BodyDef) sm.getItemDef(itemDef)).weaponSlot;
	}
	
	public List<Slot> getBodySlots() {
		return ((BodyDef) sm.getItemDef(itemDef)).bodySlots;
	}
	
	public double getRating() {
		if (armor != null)
			return armor.getRating();
		else
			return ((BodyDef) sm.getItemDef(itemDef)).rating;
	}
}

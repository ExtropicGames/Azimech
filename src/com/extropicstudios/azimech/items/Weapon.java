package com.extropicstudios.azimech.items;

import com.extropicstudios.azimech.singletons.SimpleManager;

public class Weapon extends Equipment {

	private static final SimpleManager sm = SimpleManager.getInstance(); 
	
	public Weapon() {
		super();
	}
	
	public double getPower() {
		return ((WeaponDef) sm.getItemDef(itemDef)).power;
	}
	
	public double getAccuracy() {
		return ((WeaponDef) sm.getItemDef(itemDef)).accuracy;
	}
	
	public String getAccuracySkill() {
		return ((WeaponDef) sm.getItemDef(itemDef)).accuracySkill;
	}
	
	public String getPowerSkill() {
		return ((WeaponDef) sm.getItemDef(itemDef)).powerSkill;
	}
}

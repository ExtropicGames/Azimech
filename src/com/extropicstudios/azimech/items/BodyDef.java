package com.extropicstudios.azimech.items;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 * 
 * @author josh
 */
public class BodyDef extends EquipmentDef {
	
	@Element(name="armorslot", required=false)
	public Slot armorSlot;
	
	@Element(name="weaponslot", required=false)
	public Slot weaponSlot;
	
	@ElementList(entry="bodyslot", inline=true, required=false)
	public List<Slot> bodySlots;
	
	@Element
	public double rating;
	
	/**
	 * True if this body part is required for the creature
	 * to continue living. (heart, brain, etc.)
	 */
	@Element(required=false)
	public boolean lifeCritical;
}

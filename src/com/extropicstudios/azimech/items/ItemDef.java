package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="properties")
public class ItemDef {

	/** The name the item is referred to by */
	@Element
	public String name;
	/** A description of the item */
	@Element
	public String description;
	/** The weight of the item in kilograms */
	@Element
	public float weight;
}

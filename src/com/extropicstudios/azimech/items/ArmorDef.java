package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Element;

/**
 * An extension of {@link EquipmentDef} that adds three attributes:<br>
 * <br>
 * <b>double</b> rating<br>
 *   Determines the quality of the armor.<br>
 * <b>double</b> mobility<br>
 *   Determines the chance of evading attacks while wearing the armor.<br>
 * <b>String</b> skill<br>
 *   Determines what skill influences use of the armor.<br>
 * 
 * @author josh
 */
public class ArmorDef extends EquipmentDef {

	@Element
	public double rating;
	@Element
	public double mobility;
	@Element
	public String skill;
}

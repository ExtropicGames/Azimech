package com.extropicstudios.azimech.items;

import org.newdawn.slick.util.Log;
import org.simpleframework.xml.Attribute;

import com.extropicstudios.azimech.singletons.SimpleManager;

public class Item {
	
	private static final SimpleManager sm = SimpleManager.getInstance();

	@Attribute(name="ID")
	protected String itemDef;
	@Attribute(required=false)
	protected int quantity;
	
	public String toString() {
		return "{Item, Def:"+itemDef+", Quantity:"+quantity+"}";
	}
	
	public Item() {
		quantity = 1;
	}
	
	public void setItemDef(final String itemDef) {
		this.itemDef = itemDef;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public String getName() {
		// TODO: this try catch logic should be moved
		// to a validation step that occurs once when
		// the data is loaded
		try {
			return sm.getItemDef(itemDef).name;
		} catch (NullPointerException e) {
			Log.error("Undefined item " + itemDef);
			return "";
		}
	}
	
	public String getDescription() {
		return sm.getItemDef(itemDef).description;
	}
	
	public float getWeight() {
		return sm.getItemDef(itemDef).weight;
	}
}

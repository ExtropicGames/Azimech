package com.extropicstudios.azimech.items;

import java.util.ArrayList;
import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class Inventory {

	/** The items in this inventory. */
	@ElementList
	private List<Item> items;
	
	/** The capacity of the inventory in kilograms. */
	@Element
	private float capacity;
	
	public Inventory() {
		items = new ArrayList<Item>();
	}

	public void setCapacity(float capacity) {
		this.capacity = capacity;
	}

	public float getCapacity() {
		return capacity;
	}
}

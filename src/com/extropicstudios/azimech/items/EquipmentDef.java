package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Element;

public abstract class EquipmentDef extends ItemDef {

	/** Determines which types of slots this equipment can go into. */
	@Element
	public String type;
	
	/** Determines how much damage this item can take before breaking */
	@Element
	public double durability;
	
	/** Whether to show this item on the equipment list */
	@Element(required=false)
	public boolean hidden;
}

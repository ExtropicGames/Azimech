package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Attribute;

public class Slot {

	@Attribute(name="num")
	public int number;
	@Attribute
	public String type;
	
	public String toString() {
		return "{Slot #" + number + ", " + type +"}";
	}
}

package com.extropicstudios.azimech.items;

import org.simpleframework.xml.Attribute;

import com.extropicstudios.azimech.singletons.SimpleManager;

public abstract class Equipment extends Item {

	public static final SimpleManager sm = SimpleManager.getInstance();
	
	@Attribute(required=false)
	protected double damage;

	public Equipment() {super();}
	
	public String toString() {
		return "{Equipment, " + super.toString() + ", children}";
	}
	
	public boolean isDestroyed() {
		return (damage >= getDurability());
	}
	
	/**
	 * Adds the specified amount of damage to the item's
	 * existing damage total. Any damage greater than
	 * the item's durability is returned.
	 * @param damage - The amount of damage dealt
	 * @return The excess amount of damage.
	 */
	public double takeDamage(final double damage) {
		this.damage += damage;
		final double durability = getDurability();
		if (this.damage > durability)
			return this.damage - durability;
		return 0;
	}
	
	public double getDamage() {
		return damage;
	}
	
	public void setDamage(final double damage) {
		this.damage = damage;
	}
	
	public String getType() {
		return ((EquipmentDef) sm.getItemDef(itemDef)).type;
	}

	public double getDurability() {
		return ((EquipmentDef) sm.getItemDef(itemDef)).durability;
	}
	
	public double getRemainingDurability() {
		return getDurability() - getDamage();
	}
	
	public boolean isHidden() {
		return ((EquipmentDef) sm.getItemDef(itemDef)).hidden;
	}
}

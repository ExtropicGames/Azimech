package com.extropicstudios.azimech.items;

import com.extropicstudios.azimech.singletons.SimpleManager;

/**
 * An Armor item is a type of {@link Equipment} that is
 * a specific instance of an {@link ArmorDef}.
 * @author josh
 */
public class Armor extends Equipment {

	private static final SimpleManager sm = SimpleManager.getInstance();
	
	public Armor() {
		super();
	}
	
	public double getRating() {
		return ((ArmorDef) sm.getItemDef(itemDef)).rating;
	}
	
	public double getMobility() {
		return ((ArmorDef) sm.getItemDef(itemDef)).mobility;
	}
	
	public String getSkill() {
		return ((ArmorDef) sm.getItemDef(itemDef)).skill;
	}
}
